<?php

namespace Database\Factories;

use App\Enums\FileVisibility;
use App\Models\File;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class FileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = File::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $filename = $this->faker->uuid();

        $user = User::factory();

        return [
            'filename' => $filename.'.jpg',
            'path' => 'public/'.$filename.'.jpg',
            'thumbnail_path' => 'public/'.$filename.'.jpg',
            'mime_type' => 'image/jpeg',
            'visibility_id' => FileVisibility::VISIBLE['id'],
            'description' => $this->faker->sentence(10),
            'user_id' => $user,
            'created_by' => $user,
            'updated_by' => $user,
        ];
    }
}
