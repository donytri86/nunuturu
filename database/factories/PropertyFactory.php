<?php

namespace Database\Factories;

use App\Enums\PropertyStatus;
use App\Models\Property;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PropertyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Property::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->company();

        $user = User::factory();

        return [
            'slug' => Str::slug($title),
            'title' => $title,
            'description' => $this->faker->sentence(10),
            'status_id' => PropertyStatus::AVAILABLE['id'],
            'address_1' => $this->faker->streetAddress(),
            'address_2' => $this->faker->address(),
            'city' => $this->faker->city(),
            'state' => $this->faker->state(),
            'zip_code' => $this->faker->postcode(),
            'long' => $this->faker->longitude(),
            'lat' => $this->faker->latitude(),
            'user_id' => $user,
            'created_by' => $user,
            'updated_by' => $user,
        ];
    }
}
