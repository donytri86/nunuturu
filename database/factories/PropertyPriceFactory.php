<?php

namespace Database\Factories;

use App\Enums\Period;
use App\Models\Property;
use App\Models\PropertyPrice;
use Illuminate\Database\Eloquent\Factories\Factory;

class PropertyPriceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PropertyPrice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'property_id' => Property::factory(),
            'period_id' => Period::MONTHLY['id'],
            'price' => $this->faker->numberBetween(500, 3000),
        ];
    }
}
