<?php

namespace Database\Factories;

use App\Enums\Contact;
use App\Models\User;
use App\Models\UserContact;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserContact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::factory();

        return [
            'user_id' => $user,
            'contact_id' => Contact::PHONE['id'],
            'contact' => $this->faker->phoneNumber(),
            'created_by' => $user,
            'updated_by' => $user,
        ];
    }
}
