<?php

namespace Database\Factories;

use App\Enums\Period;
use App\Models\Property;
use App\Models\Rent;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'property_id' => Property::factory(),
            'user_id' => User::factory(),
            'rent_date' => now(),
            'rent_period' => $this->faker->numberBetween(1, 6),
            'period_id' => Period::MONTHLY['id'],
            'price' => $this->faker->numberBetween(500, 3000),
        ];
    }
}
