<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table
                ->id();

            $table
                ->string('slug')
                ->unique();

            $table
                ->string('title');

            $table
                ->text('description');

            $table
                ->unsignedTinyInteger('status_id')
                ->default(0);

            $table
                ->text('address_1');

            $table
                ->text('address_2')
                ->nullable();

            $table
                ->string('city');

            $table
                ->string('state');

            $table
                ->string('zip_code');

            $table
                ->decimal('long', 16, 10)
                ->nullable();

            $table
                ->decimal('lat', 16, 10)
                ->nullable();

            $table
                ->foreignId('user_id')
                ->nullable()
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table
                ->foreignId('created_by')
                ->nullable()
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table
                ->foreignId('updated_by')
                ->nullable()
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
