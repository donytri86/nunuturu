<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_prices', function (Blueprint $table) {
            $table
                ->id();

            $table
                ->foreignId('property_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table
                ->unsignedTinyInteger('period_id')
                ->default(0);

            $table
                ->decimal('price', 16, 2);

            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_prices');
    }
}
