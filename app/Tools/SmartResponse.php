<?php

namespace App\Tools;

use Illuminate\Http\Request;
use Illuminate\Container\Container;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * SmartResponse
 */
class SmartResponse
{
    private $draw = null;
    private $code = 200;
    private $message;
    private $data = [];
    private $view = null;
    private $redirect = null;
    private $recordsFiltered = null;
    private $recordsTotal = null;
    private $paperSize = 'A4';
    private $paperOrientation = 'portrait';

    /**
     * setDraw
     * @param int $data
     */
    public function setDraw(int $data): void
    {
        $this->draw = $data;
    }

    /**
     * setCode
     * @param int $data
     */
    public function setCode(int $data): void
    {
        $this->code = $data;
    }

    /**
     * setMessage
     * @param string $data
     */
    public function setMessage(string $data): void
    {
        $this->message = $data;
    }

    /**
     * setData
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * setView
     * @param string $data
     */
    public function setView(string $data): void
    {
        $this->view = $data;
    }

    /**
     * setRedirect
     * @param string $data
     */
    public function setRedirect(string $data): void
    {
        $this->redirect = $data;
    }

    /**
     * setRecordsFiltered
     * @param int $data
     */
    public function setRecordsFiltered(int $data): void
    {
        $this->recordsFiltered = $data;
    }

    /**
     * setRecordsTotal
     * @param int $data
     */
    public function setRecordsTotal(int $data): void
    {
        $this->recordsTotal = $data;
    }

    /**
     * setPaperSize
     * @param string $data
     */
    public function setPaperSize(string $data): void
    {
        $this->paperSize = $data;
    }

    /**
     * setPaperOrientation
     * @param string $data
     */
    public function setPaperOrientation(string $data): void
    {
        $this->paperOrientation = $data;
    }

    public function render($forceJson = false, $renderViewAsJson = false)
    {
        if (request()->wantsJson() || $forceJson) {
            return $this->renderJson($renderViewAsJson);
        } elseif (! is_null($this->redirect)) {
            return $this->renderRedirect();
        } else {
            return $this->renderHtml();
        }
    }

    private function renderJson($renderViewAsJson = false)
    {
        if (is_null($this->data)) {
            $this->data = [];
        }

        $json = [];

        if (! is_null($this->draw)) {
            $json['draw'] = $this->draw;
        }

        if (! is_null($this->code)) {
            $json['code'] = $this->code;
        }

        if (! is_null($this->message)) {
            $json['message'] = $this->message;
        }

        // if (! empty($this->data)) {
            if ($renderViewAsJson === true && $this->view != null && view()->exists($this->view)) {
                $json['data'] = view($this->view, $this->data)->render();
            } else {
                $json['data'] = $this->data;
            }
        // }

        if (! is_null($this->recordsFiltered)) {
            $json['recordsFiltered'] = $this->recordsFiltered;
        }

        if (! is_null($this->recordsTotal)) {
            $json['recordsTotal'] = $this->recordsTotal;
        }

        return response()->json($json, $this->code);
    }

    private function renderHtml()
    {
        if (is_null($this->data)) {
            $this->data = [];
        }

        if ($this->view == null) {
            return abort(503, 'View has not specified.');
        } elseif (! view()->exists($this->view)) {
            return abort(404);
        } else {
            return view($this->view, $this->data);
        }
    }

    private function renderRedirect()
    {
        if ($this->redirect == '') {
            return redirect()
                ->back()
                ->withInput()
                ->with($this->data);
        } else {
            return redirect($this->redirect)
                ->withInput()
                ->with($this->data);
        }
    }

    // public function renderPDF($filename = 'output', $forceDownload = false, $pdf_data = [])
    // {
    //     if ($this->view == null) {
    //         return abort(503, 'View has not specified.');
    //     } elseif (! view()->exists($this->view)) {
    //         return abort(404);
    //     } else {
    //         $pdf = PDF::loadView($this->view, $this->data)
    //             ->setPaper($this->paperSize, $this->paperOrientation);

    //         if ($forceDownload === true) {
    //             return $pdf->download($filename.'.pdf');
    //         } elseif ($forceDownload === 'save_file') {
    //             $pdf = Storage::put(($pdf_data['path'] ?? 'default_raport_directory').'/'.$filename, $pdf->output());

    //             if ($pdf) {
    //                 $user = Auth::user();

    //                 $fileParam = new FileParam;
    //                 $fileParam->setFilename($filename);
    //                 $fileParam->setPath($pdf_data['path'] ?? 'default_raport_directory');
    //                 $fileParam->setMimetype('application/pdf');
    //                 $fileParam->setPrivateFile(1);
    //                 $fileParam->setAllowedRole(-1);
    //                 $fileParam->setOwnerId($pdf_data['owner_id'] ?? $user->id);
    //                 $fileParam->setSchoolId($pdf_data['school_id'] ?? $user->school_id);

    //                 $file = with(new Container)->call(
    //                     [
    //                         new FileRepository,
    //                         'createOrUpdate'
    //                     ],
    //                     [
    //                         'fileParam' => $fileParam
    //                     ]
    //                 );
    //             } else {
    //                 $file = null;
    //             }

    //             return $file;
    //         } else {
    //             return $pdf->stream($filename.'.pdf');
    //         }
    //     }
    // }

    public function resetData()
    {
        $this->draw = null;
        $this->code = 200;
        $this->message;
        $this->data = [];
        $this->view = null;
        $this->redirect = null;
        $this->recordsFiltered = null;
        $this->recordsTotal = null;
        $this->paperSize = 'A4';
        $this->paperOrientation = 'portrait';
    }
}
