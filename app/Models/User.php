<?php

namespace App\Models;

use App\Enums\UserStatus;
use App\Traits\EnumTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, EnumTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'status_id',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Enum casts
     */
    protected static $enum_casts = [
        'status' => 'status_id'
    ];

    /**
     * Enum attributes
     *
     * @var array
     */
    protected static $enums = [
        'status' => [
            UserStatus::INACTIVE,
            UserStatus::ACTIVE,
            UserStatus::BANNED,
        ]
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, UserRole::class);
    }

    public function contacts()
    {
        return $this->hasMany(UserContact::class);
    }

    public function rents()
    {
        return $this->hasMany(Rent::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
