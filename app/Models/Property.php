<?php

namespace App\Models;

use App\Enums\Period;
use App\Enums\PropertyStatus;
use App\Traits\EnumTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory, EnumTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'title',
        'description',
        'status_id',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip_code',
        'long',
        'lat',
        'user_id',
    ];

    /**
     * Enum casts
     */
    protected static $enum_casts = [
        'period_type' => 'period_id',
        'status' => 'status_id'
    ];

    /**
     * Enum attributes
     *
     * @var array
     */
    protected static $enums = [
        'period_type' => [
            Period::DAILY,
            Period::MONTHLY,
            Period::ANNUALY,
        ],
        'status' => [
            PropertyStatus::AVAILABLE,
            PropertyStatus::RENT,
        ]
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function created_by_user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updated_by_user()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function features()
    {
        return $this->belongsToMany(Feature::class, PropertyFeature::class);
    }

    public function galleries()
    {
        return $this->belongsToMany(File::class, PropertyGallery::class);
    }

    public function prices()
    {
        return $this->hasMany(PropertyPrice::class);
    }

    public function rents()
    {
        return $this->hasMany(Rent::class);
    }
}
