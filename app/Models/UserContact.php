<?php

namespace App\Models;

use App\Enums\Contact;
use App\Traits\EnumTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    use HasFactory, EnumTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contact',
        'contact_id',
    ];

    /**
     * Enum casts
     */
    protected static $enum_casts = [
        'contact_type' => 'contact_id'
    ];

    /**
     * Enum attributes
     *
     * @var array
     */
    protected static $enums = [
        'contact_type' => [
            Contact::PHONE,
            Contact::FACEBOOK,
            Contact::INSTAGRAM,
        ]
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function created_by_user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updated_by_user()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
