<?php

namespace App\Models;

use App\Enums\Period;
use App\Traits\EnumTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    use HasFactory, EnumTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rent_date',
        'rent_period',
        'period_id',
        'price',
    ];

    /**
     * Enum casts
     */
    protected static $enum_casts = [
        'period_type' => 'period_id'
    ];

    /**
     * Enum attributes
     *
     * @var array
     */
    protected static $enums = [
        'period_type' => [
            Period::DAILY,
            Period::MONTHLY,
            Period::ANNUALY,
        ]
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
