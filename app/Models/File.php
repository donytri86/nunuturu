<?php

namespace App\Models;

use App\Enums\FileVisibility;
use App\Traits\EnumTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory, EnumTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename',
        'path',
        'thumbnail_path',
        'mime_type',
        'visibility_id',
        'description',
        'user_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Enum casts
     */
    protected static $enum_casts = [
        'visibility' => 'visibility_id'
    ];

    /**
     * Enum attributes
     *
     * @var array
     */
    protected static $enums = [
        'visibility' => [
            FileVisibility::VISIBLE,
            FileVisibility::INVISIBLE,
        ]
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function created_by_user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updated_by_user()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
