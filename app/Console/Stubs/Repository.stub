<?php

namespace DummyNamespace;

use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use DummyNamespace\Contract\DummyClassRepositoryContract;
use DummyNamespace\GeneralSearchParam;
use DummyNamespace\DummyClassParam;
use App\Models\DummyClass;

/**
 * Repository DummyClassRepository
 * @package App\Http\Repositories
 */
class DummyClassRepository implements DummyClassRepositoryContract
{
    /**
     * Get All Data
     * @param DummyClass $dummyClass
     * @return Collection
     */
    public function getAll(DummyClass $dummyClass): Collection
    {
        return $dummyClass->get();
    }

    /**
     * Get Data By ID
     * @param $id
     * @param DummyClass $dummyClass
     * @return DummyClass|null
     */
    public function getById($id, DummyClass $dummyClass): ?DummyClass
    {
        return $dummyClass->find($id);
    }

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param DummyClass $dummyClass
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, DummyClass $dummyClass): int
    {
        return $dummyClass->count();
    }

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param DummyClass $dummyClass
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, DummyClass $dummyClass): int
    {
        $dummyClass = $this->getGeneralSearch($generalSearchParam, $dummyClass);

        return $dummyClass->count();
    }

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param DummyClass $dummyClass
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, DummyClass $dummyClass): Collection
    {
        $data = $this->getGeneralSearch($generalSearchParam, $dummyClass);

        $data = $data
            ->limit($generalSearchParam->getLength())
            ->offset($generalSearchParam->getStart())
            ->orderBy($generalSearchParam->getOrderColumn(), $generalSearchParam->getOrderDirection())
            ->get();

        return $data;
    }

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param DummyClass $dummyClass
     * @return DummyClass
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, DummyClass $dummyClass)
    {
        $dummyClass = $dummyClass
            ->with([])
            ->when(! empty($generalSearchParam->getQuery()), function ($q) use ($generalSearchParam) {
                $q->where(function ($q) use ($generalSearchParam) {
                    $q
                        ->where('column1', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('column2', 'like', "%{$generalSearchParam->getQuery()}%");
                });
            });

        return $dummyClass;
    }

    /**
     * Create New Data
     * @param  DummyClassParam $dummyClassParam
     * @param  DummyClass $dummyClass
     * @return DummyClass|null
     */
    public function create(DummyClassParam $dummyClassParam, DummyClass $dummyClass): ?DummyClass
    {
        try {
            $dummyClass->column1 = $dummyClassParam->getColumn1();
            $dummyClass->column2 = $dummyClassParam->getColumn2();

            $dummyClass->save();

            return $dummyClass;
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Update Data By ID
     * @param  int $id
     * @param  DummyClassParam $dummyClassParam
     * @param  DummyClass $dummyClass
     * @return DummyClass|null
     */
    public function updateById(int $id, DummyClassParam $dummyClassParam, DummyClass $dummyClass): ?DummyClass
    {
        try {
            $dummyClass = $this->getById($id, $dummyClass);

            if ($dummyClass != null) {
                $dummyClass = $this->create($dummyClassParam, $dummyClass);

                return $dummyClass;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  DummyClass $dummyClass
     * @return bool
     */
    public function deleteById(int $id, DummyClass $dummyClass): bool
    {
        try {
            $dummyClass = $this->getById($id, $dummyClass);

            if ($dummyClass != null) {
                $dummyClass->delete();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
