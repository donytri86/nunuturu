<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $repository_classes = [
            'Feature',
            'File',
            'PropertyPrice',
            'Property',
            'Rent',
            'Role',
            'UserContact',
            'User',
        ];

        foreach ($repository_classes as $repository_class) {
            $this->app->bind(
                "App\\Http\\Repositories\\Contract\\{$repository_class}RepositoryContract",
                "App\\Http\\Repositories\\{$repository_class}Repository"
            );
        }

        $service_classes = [
            'Feature',
            'File',
            'Property',
            'Rent',
            'Role',
            'User',
        ];

        foreach ($service_classes as $service_class) {
            $this->app->bind(
                "App\\Http\\Services\\Contract\\{$service_class}ServiceContract",
                "App\\Http\\Services\\{$service_class}Service"
            );
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
