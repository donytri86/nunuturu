<?php

namespace App\Http\Services;

use App\Http\Params\FileParam;
use App\Http\Params\GeneralSearchParam;
use App\Http\Repositories\FileRepository;
use App\Http\Services\BaseService;
use App\Http\Services\Contract\FileServiceContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Socialite;

/**
 * FileService
 */
class FileService extends BaseService implements FileServiceContract
{
    /**
     * Get File List
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        FileRepository $fileRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$fileRepository, 'getRecordsData'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            $this->total = $this->getContainer()->call(
                [$fileRepository, 'getRecordsTotal'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            $this->filtered = $this->getContainer()->call(
                [$fileRepository, 'getRecordsFiltered'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Register new File data
     */
    public function create(
        FileParam $fileParam,
        FileRepository $fileRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$fileRepository, 'create'],
                [
                    'fileParam' => $fileParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Create File Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Update File data
     */
    public function update(
        int $id,
        FileParam $fileParam,
        FileRepository $fileRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$fileRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("File Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$fileRepository, 'updateById'],
                [
                    'id' => $id,
                    'fileParam' => $fileParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Update File Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Get one File data by File ID or Filename
     */
    public function show(
        int $id = null,
        FileParam $fileParam,
        FileRepository $fileRepository
    )
    {
        try {
            if ($fileParam->getFilename()) {
                $this->data = $this->getContainer()->call(
                    [$fileRepository, 'getById'],
                    [
                        'filename' => $fileParam->getByFilename()
                    ]
                );
            } else {
                $this->data = $this->getContainer()->call(
                    [$fileRepository, 'getById'],
                    [
                        'id' => $id
                    ]
                );
            }

            if (!$this->data) {
                throw new \Exception("File Not Found", 404);
            }

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Delete one File data by File ID
     */
    public function delete(
        int $id,
        FileRepository $fileRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$fileRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("File Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$fileRepository, 'deleteById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Delete File Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }
}
