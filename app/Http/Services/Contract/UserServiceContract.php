<?php

namespace App\Http\Services\Contract;

use App\Http\Params\ArrayParam;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\UserParam;
use App\Http\Repositories\UserContactRepository;
use App\Http\Repositories\UserRepository;
use App\Models\User;
use Illuminate\Support\Collection;

/**
 * Interface UserServiceContract
 * @package App\Http\Services\Contracts
 */
interface UserServiceContract
{
    /**
     * Get User List
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        UserRepository $userRepository
    );

    /**
     * Register new User data
     */
    public function create(
        UserParam $userParam,
        UserRepository $userRepository
    );

    /**
     * Update User data
     */
    public function update(
        int $id,
        UserParam $userParam,
        ArrayParam $userContactParams,
        UserRepository $userRepository,
        UserContactRepository $userContactRepository
    );

    /**
     * Update User password
     */
    public function updatePassword(
        int $id,
        UserParam $userParam,
        UserRepository $userRepository
    );

    /**
     * Get one User data by User ID
     */
    public function show(
        int $id,
        UserParam $userParam,
        UserRepository $userRepository
    );

    /**
     * Delete one User data by User ID
     */
    public function delete(
        int $id,
        UserRepository $userRepository
    );

    /**
     * Set roles to User
     */
    public function setUserRole(
        int $id,
        $role_ids,
        UserRepository $userRepository
    );
}
