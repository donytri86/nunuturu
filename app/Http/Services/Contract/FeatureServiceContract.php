<?php

namespace App\Http\Services\Contract;

use App\Http\Params\FeatureParam;
use App\Http\Params\GeneralSearchParam;
use App\Http\Repositories\FeatureRepository;
use App\Models\Feature;
use Illuminate\Support\Collection;

/**
 * Interface FeatureServiceContract
 * @package App\Http\Services\Contracts
 */
interface FeatureServiceContract
{
    /**
     * Get Feature List
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        FeatureRepository $featureRepository
    );

    /**
     * Register new Feature data
     */
    public function create(
        FeatureParam $featureParam,
        FeatureRepository $featureRepository
    );

    /**
     * Update Feature data
     */
    public function update(
        int $id,
        FeatureParam $featureParam,
        FeatureRepository $featureRepository
    );

    /**
     * Get one Feature data by Feature ID
     */
    public function show(
        int $id,
        FeatureParam $featureParam,
        FeatureRepository $featureRepository
    );

    /**
     * Delete one Feature data by Feature ID
     */
    public function delete(
        int $id,
        FeatureRepository $featureRepository
    );
}
