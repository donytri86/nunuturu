<?php

namespace App\Http\Services\Contract;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RoleParam;
use App\Http\Repositories\RoleRepository;
use App\Models\Role;
use Illuminate\Support\Collection;

/**
 * Interface RoleServiceContract
 * @package App\Http\Services\Contracts
 */
interface RoleServiceContract
{
    /**
     * Get Role List
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        RoleRepository $roleRepository
    );

    /**
     * Register new Role data
     */
    public function create(
        RoleParam $roleParam,
        RoleRepository $roleRepository
    );

    /**
     * Update Role data
     */
    public function update(
        int $id,
        RoleParam $roleParam,
        RoleRepository $roleRepository
    );

    /**
     * Get one Role data by Role ID
     */
    public function show(
        int $id,
        RoleParam $roleParam,
        RoleRepository $roleRepository
    );

    /**
     * Delete one Role data by Role ID
     */
    public function delete(
        int $id,
        RoleRepository $roleRepository
    );
}
