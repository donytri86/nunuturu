<?php

namespace App\Http\Services\Contract;

use App\Http\Params\ArrayParam;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\PropertyParam;
use App\Http\Repositories\PropertyPriceRepository;
use App\Http\Repositories\PropertyRepository;
use App\Models\Property;
use Illuminate\Support\Collection;

/**
 * Interface PropertyServiceContract
 * @package App\Http\Services\Contracts
 */
interface PropertyServiceContract
{
    /**
     * Get Property List by Owner ID, City, State, or All Property
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        PropertyParam $propertyParam,
        PropertyRepository $propertyRepository
    );

    /**
     * Register new Property data
     */
    public function create(
        PropertyParam $propertyParam,
        ArrayParam $propertyPriceParams,
        PropertyRepository $propertyRepository,
        PropertyPriceRepository $propertyPriceRepository
    );

    /**
     * Update Property data
     */
    public function update(
        int $id,
        PropertyParam $propertyParam,
        ArrayParam $propertyPriceParams,
        PropertyRepository $propertyRepository,
        PropertyPriceRepository $propertyPriceRepository
    );

    /**
     * Get one Property data by Property ID
     */
    public function show(
        int $id,
        PropertyParam $propertyParam,
        PropertyRepository $propertyRepository
    );

    /**
     * Delete one Property data by Property ID
     */
    public function delete(
        int $id,
        PropertyRepository $propertyRepository
    );
}
