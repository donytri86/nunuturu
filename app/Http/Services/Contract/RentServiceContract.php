<?php

namespace App\Http\Services\Contract;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RentParam;
use App\Http\Repositories\RentRepository;
use App\Models\Rent;
use Illuminate\Support\Collection;

/**
 * Interface RentServiceContract
 * @package App\Http\Services\Contracts
 */
interface RentServiceContract
{
    /**
     * Get Rent List by Customer, Owner, or Property ID
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        RentParam $rentParam,
        RentRepository $rentRepository
    );

    /**
     * Register new Rent data
     */
    public function create(
        RentParam $rentParam,
        RentRepository $rentRepository
    );

    /**
     * Update Rent data
     */
    public function update(
        int $id,
        RentParam $rentParam,
        RentRepository $rentRepository
    );

    /**
     * Get one Rent data by Rent ID
     */
    public function show(
        int $id,
        RentParam $rentParam,
        RentRepository $rentRepository
    );

    /**
     * Delete one Rent data by Rent ID
     */
    public function delete(
        int $id,
        RentRepository $rentRepository
    );
}
