<?php

namespace App\Http\Services\Contract;

use App\Http\Params\FileParam;
use App\Http\Params\GeneralSearchParam;
use App\Http\Repositories\FileRepository;
use App\Models\File;
use Illuminate\Support\Collection;

/**
 * Interface FileServiceContract
 * @package App\Http\Services\Contracts
 */
interface FileServiceContract
{
    /**
     * Get File List
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        FileRepository $fileRepository
    );

    /**
     * Register new File data
     */
    public function create(
        FileParam $fileParam,
        FileRepository $fileRepository
    );

    /**
     * Update File data
     */
    public function update(
        int $id,
        FileParam $fileParam,
        FileRepository $fileRepository
    );

    /**
     * Get one File data by File ID or Filename
     */
    public function show(
        int $id = null,
        FileParam $fileParam,
        FileRepository $fileRepository
    );

    /**
     * Delete one File data by File ID
     */
    public function delete(
        int $id,
        FileRepository $fileRepository
    );
}
