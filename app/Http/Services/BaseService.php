<?php

namespace App\Http\Services;

use App\Tools\SmartResponse;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\DB;

/**
 * Base Service
 */
class BaseService
{
    private $container;
    private $smartResponse;

    public function __construct()
    {
        if ($this->container == null) {
            $this->container = new Container;
        }

        if ($this->smartResponse == null) {
            $this->smartResponse = new SmartResponse;
        }
    }

    protected function getContainer(): Container
    {
        return $this->container;
    }

    protected function dbBegin()
    {
        return DB::beginTransaction();
    }

    protected function dbCommit()
    {
        return DB::commit();
    }

    protected function dbRollback()
    {
        return DB::rollback();
    }

    protected function returnError(\Exception $e)
    {
        $status_code = $e->getCode();
        if ($status_code == 0) {
            $status_code = 500;
        }

        if (request()->wantsJson()) {
            $this->smartResponse->setCode($status_code);
            $this->smartResponse->setMessage($e->getMessage());
            return $this->smartResponse->render(true);
        } else {
            return abort($status_code, $e->getMessage());
        }
    }
}
