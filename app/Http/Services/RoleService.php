<?php

namespace App\Http\Services;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RoleParam;
use App\Http\Repositories\RoleRepository;
use App\Http\Services\BaseService;
use App\Http\Services\Contract\RoleServiceContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Socialite;

/**
 * RoleService
 */
class RoleService extends BaseService implements RoleServiceContract
{
    /**
     * Get Role List
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        RoleRepository $roleRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$roleRepository, 'getRecordsData'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            $this->total = $this->getContainer()->call(
                [$roleRepository, 'getRecordsTotal'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            $this->filtered = $this->getContainer()->call(
                [$roleRepository, 'getRecordsFiltered'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Register new Role data
     */
    public function create(
        RoleParam $roleParam,
        RoleRepository $roleRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$roleRepository, 'create'],
                [
                    'roleParam' => $roleParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Create Role Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Update Role data
     */
    public function update(
        int $id,
        RoleParam $roleParam,
        RoleRepository $roleRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$roleRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Role Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$roleRepository, 'updateById'],
                [
                    'id' => $id,
                    'roleParam' => $roleParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Update Role Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Get one Role data by Role ID
     */
    public function show(
        int $id,
        RoleParam $roleParam,
        RoleRepository $roleRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$roleRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Role Not Found", 404);
            }

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Delete one Role data by Role ID
     */
    public function delete(
        int $id,
        RoleRepository $roleRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$roleRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Role Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$roleRepository, 'deleteById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Delete Role Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }
}
