<?php

namespace App\Http\Services;

use App\Http\Params\FeatureParam;
use App\Http\Params\GeneralSearchParam;
use App\Http\Repositories\FeatureRepository;
use App\Http\Services\BaseService;
use App\Http\Services\Contract\FeatureServiceContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Socialite;

/**
 * FeatureService
 */
class FeatureService extends BaseService implements FeatureServiceContract
{
    /**
     * Get Feature List
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        FeatureRepository $featureRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$featureRepository, 'getRecordsData'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            $this->total = $this->getContainer()->call(
                [$featureRepository, 'getRecordsTotal'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            $this->filtered = $this->getContainer()->call(
                [$featureRepository, 'getRecordsFiltered'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Register new Feature data
     */
    public function create(
        FeatureParam $featureParam,
        FeatureRepository $featureRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$featureRepository, 'create'],
                [
                    'featureParam' => $featureParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Create Feature Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Update Feature data
     */
    public function update(
        int $id,
        FeatureParam $featureParam,
        FeatureRepository $featureRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$featureRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Feature Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$featureRepository, 'updateById'],
                [
                    'id' => $id,
                    'featureParam' => $featureParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Update Feature Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Get one Feature data by Feature ID
     */
    public function show(
        int $id,
        FeatureParam $featureParam,
        FeatureRepository $featureRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$featureRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Feature Not Found", 404);
            }

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Delete one Feature data by Feature ID
     */
    public function delete(
        int $id,
        FeatureRepository $featureRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$featureRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Feature Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$featureRepository, 'deleteById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Delete Feature Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }
}
