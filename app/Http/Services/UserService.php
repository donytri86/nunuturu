<?php

namespace App\Http\Services;

use App\Http\Params\ArrayParam;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\UserContactParam;
use App\Http\Params\UserParam;
use App\Http\Repositories\UserRepository;
use App\Http\Repositories\UserContactRepository;
use App\Http\Services\BaseService;
use App\Http\Services\Contract\UserServiceContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Socialite;

/**
 * UserService
 */
class UserService extends BaseService implements UserServiceContract
{
    /**
     * Get User List
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        UserRepository $userRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$userRepository, 'getRecordsData'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            $this->total = $this->getContainer()->call(
                [$userRepository, 'getRecordsTotal'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            $this->filtered = $this->getContainer()->call(
                [$userRepository, 'getRecordsFiltered'],
                [
                    'generalSearchParam' => $generalSearchParam
                ]
            );

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Create multiple User Contact
     */
    private function createContact(
        $user,
        ArrayParam $userContactParams,
        UserContactRepository $userContactRepository
    )
    {
        try {
            $user->contacts()->delete();

            $contacts = new Collection();

            while ($userContactParam = $userContactParams->get()) {
                if (! $userContactParam instanceof UserContactParam) {
                    throw new \Exception("Invalid User Contact Parameter", 400);
                }

                $userContactParam->setUserId($user->id);

                $contact = $this->getContainer()->call(
                    [$userContactRepository, 'create'],
                    [
                        'userContactParam' => $userContactParam
                    ]
                );

                if (!$contact) {
                    throw new \Exception("Create User Contact Failed", 503);
                }

                $contacts->push($contact);
            }

            return $contacts;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Register new User data
     */
    public function create(
        UserParam $userParam,
        UserRepository $userRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$userRepository, 'create'],
                [
                    'userParam' => $userParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Create User Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Update User data
     */
    public function update(
        int $id,
        UserParam $userParam,
        ArrayParam $userContactParams,
        UserRepository $userRepository,
        UserContactRepository $userContactRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$userRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("User Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$userRepository, 'updateById'],
                [
                    'id' => $id,
                    'userParam' => $userParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Update User Failed", 503);
            }

            $contacts = $this->createContact($this->data, $userContactParams, $userContactRepository);

            if ($contacts instanceof \Exception) {
                throw $contacts;
            }

            $this->data->contacts = $contacts;

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Update User password
     */
    public function updatePassword(
        int $id,
        UserParam $userParam,
        UserRepository $userRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$userRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("User Not Found", 404);
            }

            $userParam->init($this->data);

            if (! Hash::check(
                $userParam->getCurrentPassword(),
                $this->data->password
            )) {
                throw new \Exception("Current Password Invalid", 401);
            }

            $this->data = $this->getContainer()->call(
                [$userRepository, 'updateById'],
                [
                    'id' => $this->data->id,
                    'userParam' => $userParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Update Password Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Get one User data by User ID
     */
    public function show(
        int $id,
        UserParam $userParam,
        UserRepository $userRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$userRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("User Not Found", 404);
            }

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Delete one User data by User ID
     */
    public function delete(
        int $id,
        UserRepository $userRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$userRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("User Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$userRepository, 'deleteById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Delete User Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Set roles to User
     */
    public function setUserRole(
        int $id,
        $role_ids,
        UserRepository $userRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$userRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("User Not Found.", 404);
            }

            $this->data->roles()->attach($role_ids);

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }
}
