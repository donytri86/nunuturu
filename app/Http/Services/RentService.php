<?php

namespace App\Http\Services;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RentParam;
use App\Http\Repositories\RentRepository;
use App\Http\Services\BaseService;
use App\Http\Services\Contract\RentServiceContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Socialite;

/**
 * RentService
 */
class RentService extends BaseService implements RentServiceContract
{
    /**
     * Get Rent List by Customer, Owner, or Property ID
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        RentParam $rentParam,
        RentRepository $rentRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$rentRepository, 'getRecordsData'],
                [
                    'generalSearchParam' => $generalSearchParam,
                    'rentParam' => $rentParam,
                ]
            );

            $this->total = $this->getContainer()->call(
                [$rentRepository, 'getRecordsTotal'],
                [
                    'generalSearchParam' => $generalSearchParam,
                    'rentParam' => $rentParam,
                ]
            );

            $this->filtered = $this->getContainer()->call(
                [$rentRepository, 'getRecordsFiltered'],
                [
                    'generalSearchParam' => $generalSearchParam,
                    'rentParam' => $rentParam,
                ]
            );

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Register new Rent data
     */
    public function create(
        RentParam $rentParam,
        RentRepository $rentRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$rentRepository, 'create'],
                [
                    'rentParam' => $rentParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Create Rent Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Update Rent data
     */
    public function update(
        int $id,
        RentParam $rentParam,
        RentRepository $rentRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$rentRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Rent Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$rentRepository, 'updateById'],
                [
                    'id' => $id,
                    'rentParam' => $rentParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Update Rent Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Get one Rent data by Rent ID
     */
    public function show(
        int $id,
        RentParam $rentParam,
        RentRepository $rentRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$rentRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Rent Not Found", 404);
            }

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Delete one Rent data by Rent ID
     */
    public function delete(
        int $id,
        RentRepository $rentRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$rentRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Rent Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$rentRepository, 'deleteById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Delete Rent Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }
}
