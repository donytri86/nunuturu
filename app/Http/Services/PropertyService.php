<?php

namespace App\Http\Services;

use App\Http\Params\ArrayParam;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\PropertyParam;
use App\Http\Params\PropertyPriceParam;
use App\Http\Repositories\PropertyPriceRepository;
use App\Http\Repositories\PropertyRepository;
use App\Http\Services\BaseService;
use App\Http\Services\Contract\PropertyServiceContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Socialite;

/**
 * PropertyService
 */
class PropertyService extends BaseService implements PropertyServiceContract
{
    /**
     * Get Property List by Owner ID, City, State, or All Property
     */
    public function list(
        GeneralSearchParam $generalSearchParam,
        PropertyParam $propertyParam,
        PropertyRepository $propertyRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$propertyRepository, 'getRecordsData'],
                [
                    'generalSearchParam' => $generalSearchParam,
                    'propertyParam' => $propertyParam,
                ]
            );

            $this->total = $this->getContainer()->call(
                [$propertyRepository, 'getRecordsTotal'],
                [
                    'generalSearchParam' => $generalSearchParam,
                    'propertyParam' => $propertyParam,
                ]
            );

            $this->filtered = $this->getContainer()->call(
                [$propertyRepository, 'getRecordsFiltered'],
                [
                    'generalSearchParam' => $generalSearchParam,
                    'propertyParam' => $propertyParam,
                ]
            );

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Create multiple Property Price
     */
    private function createPrice(
        $property,
        ArrayParam $propertyPriceParams,
        PropertyPriceRepository $propertyPriceRepository
    )
    {
        try {
            $property->prices()->delete();

            $prices = new Collection();

            while ($propertyPriceParam = $propertyPriceParams->get()) {
                if (! $propertyPriceParam instanceof PropertyPriceParam) {
                    throw new \Exception("Invalid Property Price Parameter", 400);
                }

                $propertyPriceParam->setPropertyId($property->id);

                $price = $this->getContainer()->call(
                    [$propertyPriceRepository, 'create'],
                    [
                        'propertyPriceParam' => $propertyPriceParam
                    ]
                );

                if (!$price) {
                    throw new \Exception("Create Property Price Failed", 503);
                }

                $prices->push($price);
            }

            return $prices;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Register new Property data
     */
    public function create(
        PropertyParam $propertyParam,
        ArrayParam $propertyPriceParams,
        PropertyRepository $propertyRepository,
        PropertyPriceRepository $propertyPriceRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$propertyRepository, 'create'],
                [
                    'propertyParam' => $propertyParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Create Property Failed", 503);
            }

            $this->data->galleries()->sync($propertyParam->getGalleryIds());

            $prices = $this->createPrice($this->data, $propertyPriceParams, $propertyPriceRepository);

            if ($prices instanceof \Exception) {
                throw $prices;
            }

            $this->data->prices = $prices;

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Update Property data
     */
    public function update(
        int $id,
        PropertyParam $propertyParam,
        ArrayParam $propertyPriceParams,
        PropertyRepository $propertyRepository,
        PropertyPriceRepository $propertyPriceRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$propertyRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Property Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$propertyRepository, 'updateById'],
                [
                    'id' => $id,
                    'propertyParam' => $propertyParam
                ]
            );

            if (!$this->data) {
                throw new \Exception("Update Property Failed", 503);
            }

            $this->data->galleries()->sync($propertyParam->getGalleryIds());


            $prices = $this->createPrice($this->data, $propertyPriceParams, $propertyPriceRepository);

            if ($prices instanceof \Exception) {
                throw $prices;
            }

            $this->data->prices = $prices;

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }

    /**
     * Get one Property data by Property ID
     */
    public function show(
        int $id,
        PropertyParam $propertyParam,
        PropertyRepository $propertyRepository
    )
    {
        try {
            $this->data = $this->getContainer()->call(
                [$propertyRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Property Not Found", 404);
            }

            return $this;
        } catch (\Exception $e) {
            report($e);

            return $this->returnError($e);
        }
    }

    /**
     * Delete one Property data by Property ID
     */
    public function delete(
        int $id,
        PropertyRepository $propertyRepository
    )
    {
        $this->dbBegin();

        try {
            $this->data = $this->getContainer()->call(
                [$propertyRepository, 'getById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Property Not Found", 404);
            }

            $this->data = $this->getContainer()->call(
                [$propertyRepository, 'deleteById'],
                [
                    'id' => $id
                ]
            );

            if (!$this->data) {
                throw new \Exception("Delete Property Failed", 503);
            }

            $this->dbCommit();

            return $this;
        } catch (\Exception $e) {
            report($e);

            $this->dbRollback();

            return $this->returnError($e);
        }
    }
}
