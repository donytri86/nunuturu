<?php

namespace App\Http\Params;

use App\Models\UserContact;

/**
 * UserContactParam
 */
class UserContactParam
{
    private $user_id;
    private $contact_id;
    private $contact;
    private $created_by;
    private $updated_by;

    /**
     * Init default data from model
     */
    public function init(UserContact $userContact)
    {
        $this->user_id  = $userContact->user_id;
        $this->contact_id  = $userContact->contact_id;
        $this->contact  = $userContact->contact;
        $this->created_by  = $userContact->created_by;
        $this->updated_by  = $userContact->updated_by;
    }

    /**
     * Get the value of user_id
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set the value of user_id
     */
    public function setUserId($user_id) : self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get the value of contact_id
     */
    public function getContactId()
    {
        return $this->contact_id;
    }

    /**
     * Set the value of contact_id
     */
    public function setContactId($contact_id) : self
    {
        $this->contact_id = $contact_id;

        return $this;
    }

    /**
     * Get the value of contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set the value of contact
     */
    public function setContact($contact) : self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get the value of created_by
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set the value of created_by
     */
    public function setCreatedBy($created_by) : self
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * Get the value of updated_by
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * Set the value of updated_by
     */
    public function setUpdatedBy($updated_by) : self
    {
        $this->updated_by = $updated_by;

        return $this;
    }
}
