<?php

namespace App\Http\Params;

use App\Models\Property;

/**
 * PropertyParam
 */
class PropertyParam
{
    private $slug;
    private $title;
    private $description;
    private $status_id;
    private $address_1;
    private $address_2;
    private $city;
    private $state;
    private $zip_code;
    private $long;
    private $lat;
    private $user_id;
    private $created_by;
    private $updated_by;
    private $gallery_ids = [];

    /**
     * Init default data from model
     */
    public function init(Property $property)
    {
        $this->slug = $property->slug;
        $this->title = $property->title;
        $this->description = $property->description;
        $this->status_id = $property->status_id;
        $this->address_1 = $property->address_1;
        $this->address_2 = $property->address_2;
        $this->city = $property->city;
        $this->state = $property->state;
        $this->zip_code = $property->zip_code;
        $this->long = $property->long;
        $this->lat = $property->lat;
        $this->user_id = $property->user_id;
    }

    /**
     * Get the value of slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set the value of slug
     */
    public function setSlug($slug) : self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get the value of title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     */
    public function setTitle($title) : self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     */
    public function setDescription($description) : self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of status_id
     */
    public function getStatusId()
    {
        return $this->status_id;
    }

    /**
     * Set the value of status_id
     */
    public function setStatusId($status_id) : self
    {
        $this->status_id = $status_id;

        return $this;
    }

    /**
     * Get the value of address_1
     */
    public function getAddress1()
    {
        return $this->address_1;
    }

    /**
     * Set the value of address_1
     */
    public function setAddress1($address_1) : self
    {
        $this->address_1 = $address_1;

        return $this;
    }

    /**
     * Get the value of address_2
     */
    public function getAddress2()
    {
        return $this->address_2;
    }

    /**
     * Set the value of address_2
     */
    public function setAddress2($address_2) : self
    {
        $this->address_2 = $address_2;

        return $this;
    }

    /**
     * Get the value of city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     */
    public function setCity($city) : self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of state
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set the value of state
     */
    public function setState($state) : self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get the value of zip_code
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * Set the value of zip_code
     */
    public function setZipCode($zip_code) : self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    /**
     * Get the value of long
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * Set the value of long
     */
    public function setLong($long) : self
    {
        $this->long = $long;

        return $this;
    }

    /**
     * Get the value of lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set the value of lat
     */
    public function setLat($lat) : self
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get the value of user_id
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set the value of user_id
     */
    public function setUserId($user_id) : self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get the value of gallery_ids
     */
    public function getGalleryIds()
    {
        return $this->gallery_ids;
    }

    /**
     * Set the value of gallery_ids
     */
    public function setGalleryIds($gallery_ids) : self
    {
        $this->gallery_ids = $gallery_ids;

        return $this;
    }

    /**
     * Get the value of created_by
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set the value of created_by
     */
    public function setCreatedBy($created_by) : self
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * Get the value of updated_by
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * Set the value of updated_by
     */
    public function setUpdatedBy($updated_by) : self
    {
        $this->updated_by = $updated_by;

        return $this;
    }
}
