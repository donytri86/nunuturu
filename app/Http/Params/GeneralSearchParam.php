<?php

namespace App\Http\Params;

/**
 * GeneralSearchParam
 */
class GeneralSearchParam
{
    private $query;
    private $length = 10;
    private $start = 0;
    private $order_column = 'id';
    private $order_direction = 'desc';

    /**
     * Get the value of query
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set the value of query
     *
     * @return  self
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get the value of length
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set the value of length
     *
     * @return  self
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get the value of start
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set the value of start
     *
     * @return  self
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get the value of order_column
     */
    public function getOrderColumn()
    {
        return $this->order_column;
    }

    /**
     * Set the value of order_column
     *
     * @return  self
     */
    public function setOrderColumn($order_column)
    {
        $this->order_column = $order_column;

        return $this;
    }

    /**
     * Get the value of order_direction
     */
    public function getOrderDirection()
    {
        return $this->order_direction;
    }

    /**
     * Set the value of order_direction
     *
     * @return  self
     */
    public function setOrderDirection($order_direction)
    {
        $this->order_direction = $order_direction;

        return $this;
    }
}
