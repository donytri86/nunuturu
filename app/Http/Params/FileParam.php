<?php

namespace App\Http\Params;

use App\Models\File;

/**
 * FileParam
 */
class FileParam
{
    private $filename;
    private $path;
    private $thumbnail_path;
    private $mime_type;
    private $visibility_id;
    private $description;
    private $user_id;
    private $created_by;
    private $updated_by;

    /**
     * Init default data from model
     */
    public function init(File $file)
    {
        $this->filename = $file->filename;
        $this->path = $file->path;
        $this->thumbnail_path = $file->thumbnail_path;
        $this->mime_type = $file->mime_type;
        $this->visibility_id = $file->visibility_id;
        $this->description = $file->description;
        $this->user_id = $file->user_id;
    }

    /**
     * Get the value of filename
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set the value of filename
     */
    public function setFilename($filename) : self
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get the value of path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set the value of path
     */
    public function setPath($path) : self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get the value of thumbnail_path
     */
    public function getThumbnailPath()
    {
        return $this->thumbnail_path;
    }

    /**
     * Set the value of thumbnail_path
     */
    public function setThumbnailPath($thumbnail_path) : self
    {
        $this->thumbnail_path = $thumbnail_path;

        return $this;
    }

    /**
     * Get the value of mime_type
     */
    public function getMimeType()
    {
        return $this->mime_type;
    }

    /**
     * Set the value of mime_type
     */
    public function setMimeType($mime_type) : self
    {
        $this->mime_type = $mime_type;

        return $this;
    }

    /**
     * Get the value of visibility_id
     */
    public function getVisibilityId()
    {
        return $this->visibility_id;
    }

    /**
     * Set the value of visibility_id
     */
    public function setVisibilityId($visibility_id) : self
    {
        $this->visibility_id = $visibility_id;

        return $this;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     */
    public function setDescription($description) : self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of user_id
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set the value of user_id
     */
    public function setUserId($user_id) : self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get the value of created_by
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set the value of created_by
     */
    public function setCreatedBy($created_by) : self
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * Get the value of updated_by
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * Set the value of updated_by
     */
    public function setUpdatedBy($updated_by) : self
    {
        $this->updated_by = $updated_by;

        return $this;
    }
}
