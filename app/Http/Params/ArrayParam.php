<?php

namespace App\Http\Params;

use Illuminate\Support\Collection;

/**
 * ProductGalleryParam
 */
class ArrayParam
{
    private $paramClass;
    private $data = null;

    public function __construct()
    {
        if ($this->data === null) {
            $this->data = new Collection;
        }
    }

    /**
     * Set Class of Array
     */
    public function setClass($paramClass)
    {
        $this->paramClass = $paramClass;
    }

    /**
     * get Class of Array
     */
    public function getClass()
    {
        return $this->paramClass;
    }

    /**
     * Add param item
     * @return  self
     *
     */
    public function add($param)
    {
        $this->data->push($param);

        return $this;
    }

    /**
     * Get param value
     */
    public function get()
    {
        return $this->data->shift();
    }

    /**
     * Get param count
     */
    public function count()
    {
        return $this->data->count();
    }
}
