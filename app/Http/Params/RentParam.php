<?php

namespace App\Http\Params;

use App\Models\Rent;

/**
 * RentParam
 */
class RentParam
{
    private $property_id;
    private $user_id;
    private $rent_date;
    private $rent_period;
    private $period_id;
    private $price;
    private $property_owner_id;

    /**
     * Init default data from model
     */
    public function init(Rent $rent)
    {
        $this->property_id = $rent->property_id;
        $this->user_id = $rent->user_id;
        $this->rent_date = $rent->rent_date;
        $this->rent_period = $rent->rent_period;
        $this->period_id = $rent->period_id;
        $this->price = $rent->price;
    }

    /**
     * Get the value of property_id
     */
    public function getPropertyId()
    {
        return $this->property_id;
    }

    /**
     * Set the value of property_id
     */
    public function setPropertyId($property_id) : self
    {
        $this->property_id = $property_id;

        return $this;
    }

    /**
     * Get the value of user_id
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set the value of user_id
     */
    public function setUserId($user_id) : self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get the value of rent_date
     */
    public function getRentDate()
    {
        return $this->rent_date;
    }

    /**
     * Set the value of rent_date
     */
    public function setRentDate($rent_date) : self
    {
        $this->rent_date = $rent_date;

        return $this;
    }

    /**
     * Get the value of rent_period
     */
    public function getRentPeriod()
    {
        return $this->rent_period;
    }

    /**
     * Set the value of rent_period
     */
    public function setRentPeriod($rent_period) : self
    {
        $this->rent_period = $rent_period;

        return $this;
    }

    /**
     * Get the value of period_id
     */
    public function getPeriodId()
    {
        return $this->period_id;
    }

    /**
     * Set the value of period_id
     */
    public function setPeriodId($period_id) : self
    {
        $this->period_id = $period_id;

        return $this;
    }

    /**
     * Get the value of price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of price
     */
    public function setPrice($price) : self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get the value of property_owner_id
     */
    public function getPropertyOwnerId()
    {
        return $this->property_owner_id;
    }

    /**
     * Set the value of property_owner_id
     */
    public function setPropertyOwnerId($property_owner_id) : self
    {
        $this->property_owner_id = $property_owner_id;

        return $this;
    }
}
