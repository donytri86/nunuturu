<?php

namespace App\Http\Params;

use App\Models\PropertyPrice;

/**
 * PropertyPriceParam
 */
class PropertyPriceParam
{
    private $property_id;
    private $period_id;
    private $price;

    /**
     * Init default data from model
     */
    public function init(PropertyPrice $propertyPrice)
    {
        $this->property_id = $propertyPrice->property_id;
        $this->period_id = $propertyPrice->period_id;
        $this->price = $propertyPrice->price;
    }

    /**
     * Get the value of property_id
     */
    public function getPropertyId()
    {
        return $this->property_id;
    }

    /**
     * Set the value of property_id
     */
    public function setPropertyId($property_id) : self
    {
        $this->property_id = $property_id;

        return $this;
    }

    /**
     * Get the value of period_id
     */
    public function getPeriodId()
    {
        return $this->period_id;
    }

    /**
     * Set the value of period_id
     */
    public function setPeriodId($period_id) : self
    {
        $this->period_id = $period_id;

        return $this;
    }

    /**
     * Get the value of price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of price
     */
    public function setPrice($price) : self
    {
        $this->price = $price;

        return $this;
    }
}
