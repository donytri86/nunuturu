<?php

namespace App\Http\Params;

use App\Models\Feature;

/**
 * FeatureParam
 */
class FeatureParam
{
    private $slug;
    private $name;
    private $created_by;
    private $updated_by;

    /**
     * Init default data from model
     */
    public function init(Feature $feature)
    {
        $this->slug = $feature->slug;
        $this->name = $feature->name;
    }

    /**
     * Get the value of slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set the value of slug
     */
    public function setSlug($slug) : self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     */
    public function setName($name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of created_by
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set the value of created_by
     */
    public function setCreatedBy($created_by) : self
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * Get the value of updated_by
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * Set the value of updated_by
     */
    public function setUpdatedBy($updated_by) : self
    {
        $this->updated_by = $updated_by;

        return $this;
    }
}
