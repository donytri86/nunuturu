<?php

namespace App\Http\Params;

use App\Models\User;

/**
 * UserParam
 */
class UserParam
{
    private $first_name;
    private $last_name;
    private $email;
    private $status_id;
    private $email_verified_at;
    private $password;
    private $current_password;

    /**
     * Init default data from model
     */
    public function init(User $user)
    {
        $this->first_name = $user->first_name;
        $this->last_name = $user->last_name;
        $this->email = $user->email;
        $this->status_id = $user->status_id;
    }

    /**
     * Get the value of first_name
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set the value of first_name
     */
    public function setFirstName($first_name) : self
    {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * Get the value of last_name
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set the value of last_name
     */
    public function setLastName($last_name) : self
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     */
    public function setEmail($email) : self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of status_id
     */
    public function getStatusId()
    {
        return $this->status_id;
    }

    /**
     * Set the value of status_id
     */
    public function setStatusId($status_id) : self
    {
        $this->status_id = $status_id;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     */
    public function setPassword($password) : self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of current_password
     */
    public function getCurrentPassword()
    {
        return $this->current_password;
    }

    /**
     * Set the value of current_password
     */
    public function setCurrentPassword($current_password) : self
    {
        $this->current_password = $current_password;

        return $this;
    }

    /**
     * Get the value of email_verified_at
     */
    public function getEmailVerifiedAt()
    {
        return $this->email_verified_at;
    }

    /**
     * Set the value of email_verified_at
     */
    public function setEmailVerifiedAt($email_verified_at) : self
    {
        $this->email_verified_at = $email_verified_at;

        return $this;
    }
}
