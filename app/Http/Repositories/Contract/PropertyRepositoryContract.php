<?php

namespace App\Http\Repositories\Contract;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\PropertyParam;
use App\Models\Property;
use Illuminate\Support\Collection;

/**
 * Interface PropertyRepositoryContract
 * @package App\Http\Repositories\Contracts
 */
interface PropertyRepositoryContract
{
    /**
     * Get All Data
     * @param Property $property
     * @return Collection
     */
    public function getAll(Property $property): Collection;

    /**
     * Get Data By ID
     * @param $id
     * @param Property $property
     * @return Property|null
     */
    public function getById($id, Property $property): ?Property;

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyParam $propertyParam
     * @param Property $property
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, PropertyParam $propertyParam, Property $property): int;

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyParam $propertyParam
     * @param Property $property
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, PropertyParam $propertyParam, Property $property): int;

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyParam $propertyParam
     * @param Property $property
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, PropertyParam $propertyParam, Property $property): Collection;

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyParam $propertyParam
     * @param Property $property
     * @return Property
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, PropertyParam $propertyParam, Property $property);

    /**
     * Create New Data
     * @param  PropertyParam $propertyParam
     * @param  Property $property
     * @return Property|null
     */
    public function create(PropertyParam $propertyParam, Property $property): ?Property;

    /**
     * Update Data By ID
     * @param  int $id
     * @param  PropertyParam $propertyParam
     * @param  Property $property
     * @return Property|null
     */
    public function updateById(int $id, PropertyParam $propertyParam, Property $property): ?Property;

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  Property $property
     * @return bool
     */
    public function deleteById(int $id, Property $property): bool;
}
