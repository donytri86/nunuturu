<?php

namespace App\Http\Repositories\Contract;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\UserContactParam;
use App\Models\UserContact;
use Illuminate\Support\Collection;

/**
 * Interface UserContactRepositoryContract
 * @package App\Http\Repositories\Contracts
 */
interface UserContactRepositoryContract
{
    /**
     * Get All Data
     * @param UserContact $userContact
     * @return Collection
     */
    public function getAll(UserContact $userContact): Collection;

    /**
     * Get Data By ID
     * @param $id
     * @param UserContact $userContact
     * @return UserContact|null
     */
    public function getById($id, UserContact $userContact): ?UserContact;

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param UserContact $userContact
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, UserContact $userContact): int;

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param UserContact $userContact
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, UserContact $userContact): int;

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param UserContact $userContact
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, UserContact $userContact): Collection;

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param UserContact $userContact
     * @return UserContact
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, UserContact $userContact);

    /**
     * Create New Data
     * @param  UserContactParam $userContactParam
     * @param  UserContact $userContact
     * @return UserContact|null
     */
    public function create(UserContactParam $userContactParam, UserContact $userContact): ?UserContact;

    /**
     * Update Data By ID
     * @param  int $id
     * @param  UserContactParam $userContactParam
     * @param  UserContact $userContact
     * @return UserContact|null
     */
    public function updateById(int $id, UserContactParam $userContactParam, UserContact $userContact): ?UserContact;

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  UserContact $userContact
     * @return bool
     */
    public function deleteById(int $id, UserContact $userContact): bool;
}
