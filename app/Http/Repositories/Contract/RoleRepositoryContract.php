<?php

namespace App\Http\Repositories\Contract;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RoleParam;
use App\Models\Role;
use Illuminate\Support\Collection;

/**
 * Interface RoleRepositoryContract
 * @package App\Http\Repositories\Contracts
 */
interface RoleRepositoryContract
{
    /**
     * Get All Data
     * @param Role $role
     * @return Collection
     */
    public function getAll(Role $role): Collection;

    /**
     * Get Data By ID
     * @param $id
     * @param Role $role
     * @return Role|null
     */
    public function getById($id, Role $role): ?Role;

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param Role $role
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, Role $role): int;

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param Role $role
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, Role $role): int;

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param Role $role
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, Role $role): Collection;

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param Role $role
     * @return Role
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, Role $role);

    /**
     * Create New Data
     * @param  RoleParam $roleParam
     * @param  Role $role
     * @return Role|null
     */
    public function create(RoleParam $roleParam, Role $role): ?Role;

    /**
     * Update Data By ID
     * @param  int $id
     * @param  RoleParam $roleParam
     * @param  Role $role
     * @return Role|null
     */
    public function updateById(int $id, RoleParam $roleParam, Role $role): ?Role;

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  Role $role
     * @return bool
     */
    public function deleteById(int $id, Role $role): bool;
}
