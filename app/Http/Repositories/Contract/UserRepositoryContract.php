<?php

namespace App\Http\Repositories\Contract;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\UserParam;
use App\Models\User;
use Illuminate\Support\Collection;

/**
 * Interface UserRepositoryContract
 * @package App\Http\Repositories\Contracts
 */
interface UserRepositoryContract
{
    /**
     * Get All Data
     * @param User $user
     * @return Collection
     */
    public function getAll(User $user): Collection;

    /**
     * Get Data By ID
     * @param $id
     * @param User $user
     * @return User|null
     */
    public function getById($id, User $user): ?User;

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param User $user
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, User $user): int;

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param User $user
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, User $user): int;

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param User $user
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, User $user): Collection;

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param User $user
     * @return User
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, User $user);

    /**
     * Create New Data
     * @param  UserParam $userParam
     * @param  User $user
     * @return User|null
     */
    public function create(UserParam $userParam, User $user): ?User;

    /**
     * Update Data By ID
     * @param  int $id
     * @param  UserParam $userParam
     * @param  User $user
     * @return User|null
     */
    public function updateById(int $id, UserParam $userParam, User $user): ?User;

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  User $user
     * @return bool
     */
    public function deleteById(int $id, User $user): bool;
}
