<?php

namespace App\Http\Repositories\Contract;

use App\Http\Params\FileParam;
use App\Http\Params\GeneralSearchParam;
use App\Models\File;
use Illuminate\Support\Collection;

/**
 * Interface FileRepositoryContract
 * @package App\Http\Repositories\Contracts
 */
interface FileRepositoryContract
{
    /**
     * Get All Data
     * @param File $file
     * @return Collection
     */
    public function getAll(File $file): Collection;

    /**
     * Get Data By ID
     * @param $id
     * @param File $file
     * @return File|null
     */
    public function getById($id, File $file): ?File;

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param File $file
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, File $file): int;

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param File $file
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, File $file): int;

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param File $file
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, File $file): Collection;

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param File $file
     * @return File
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, File $file);

    /**
     * Create New Data
     * @param  FileParam $fileParam
     * @param  File $file
     * @return File|null
     */
    public function create(FileParam $fileParam, File $file): ?File;

    /**
     * Update Data By ID
     * @param  int $id
     * @param  FileParam $fileParam
     * @param  File $file
     * @return File|null
     */
    public function updateById(int $id, FileParam $fileParam, File $file): ?File;

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  File $file
     * @return bool
     */
    public function deleteById(int $id, File $file): bool;
}
