<?php

namespace App\Http\Repositories\Contract;

use App\Http\Params\FeatureParam;
use App\Http\Params\GeneralSearchParam;
use App\Models\Feature;
use Illuminate\Support\Collection;

/**
 * Interface FeatureRepositoryContract
 * @package App\Http\Repositories\Contracts
 */
interface FeatureRepositoryContract
{
    /**
     * Get All Data
     * @param Feature $feature
     * @return Collection
     */
    public function getAll(Feature $feature): Collection;

    /**
     * Get Data By ID
     * @param $id
     * @param Feature $feature
     * @return Feature|null
     */
    public function getById($id, Feature $feature): ?Feature;

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param Feature $feature
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, Feature $feature): int;

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param Feature $feature
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, Feature $feature): int;

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param Feature $feature
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, Feature $feature): Collection;

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param Feature $feature
     * @return Feature
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, Feature $feature);

    /**
     * Create New Data
     * @param  FeatureParam $featureParam
     * @param  Feature $feature
     * @return Feature|null
     */
    public function create(FeatureParam $featureParam, Feature $feature): ?Feature;

    /**
     * Update Data By ID
     * @param  int $id
     * @param  FeatureParam $featureParam
     * @param  Feature $feature
     * @return Feature|null
     */
    public function updateById(int $id, FeatureParam $featureParam, Feature $feature): ?Feature;

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  Feature $feature
     * @return bool
     */
    public function deleteById(int $id, Feature $feature): bool;
}
