<?php

namespace App\Http\Repositories\Contract;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\PropertyPriceParam;
use App\Models\PropertyPrice;
use Illuminate\Support\Collection;

/**
 * Interface PropertyPriceRepositoryContract
 * @package App\Http\Repositories\Contracts
 */
interface PropertyPriceRepositoryContract
{
    /**
     * Get All Data
     * @param PropertyPrice $propertyPrice
     * @return Collection
     */
    public function getAll(PropertyPrice $propertyPrice): Collection;

    /**
     * Get Data By ID
     * @param $id
     * @param PropertyPrice $propertyPrice
     * @return PropertyPrice|null
     */
    public function getById($id, PropertyPrice $propertyPrice): ?PropertyPrice;

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyPrice $propertyPrice
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, PropertyPrice $propertyPrice): int;

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyPrice $propertyPrice
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, PropertyPrice $propertyPrice): int;

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyPrice $propertyPrice
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, PropertyPrice $propertyPrice): Collection;

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyPrice $propertyPrice
     * @return PropertyPrice
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, PropertyPrice $propertyPrice);

    /**
     * Create New Data
     * @param  PropertyPriceParam $propertyPriceParam
     * @param  PropertyPrice $propertyPrice
     * @return PropertyPrice|null
     */
    public function create(PropertyPriceParam $propertyPriceParam, PropertyPrice $propertyPrice): ?PropertyPrice;

    /**
     * Update Data By ID
     * @param  int $id
     * @param  PropertyPriceParam $propertyPriceParam
     * @param  PropertyPrice $propertyPrice
     * @return PropertyPrice|null
     */
    public function updateById(int $id, PropertyPriceParam $propertyPriceParam, PropertyPrice $propertyPrice): ?PropertyPrice;

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  PropertyPrice $propertyPrice
     * @return bool
     */
    public function deleteById(int $id, PropertyPrice $propertyPrice): bool;
}
