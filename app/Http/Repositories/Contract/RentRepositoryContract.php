<?php

namespace App\Http\Repositories\Contract;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RentParam;
use App\Models\Rent;
use Illuminate\Support\Collection;

/**
 * Interface RentRepositoryContract
 * @package App\Http\Repositories\Contracts
 */
interface RentRepositoryContract
{
    /**
     * Get All Data
     * @param Rent $rent
     * @return Collection
     */
    public function getAll(Rent $rent): Collection;

    /**
     * Get Data By ID
     * @param $id
     * @param Rent $rent
     * @return Rent|null
     */
    public function getById($id, Rent $rent): ?Rent;

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param RentParam $rentParam
     * @param Rent $rent
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, RentParam $rentParam, Rent $rent): int;

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param RentParam $rentParam
     * @param Rent $rent
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, RentParam $rentParam, Rent $rent): int;

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param RentParam $rentParam
     * @param Rent $rent
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, RentParam $rentParam, Rent $rent): Collection;

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param RentParam $rentParam
     * @param Rent $rent
     * @return Rent
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, RentParam $rentParam, Rent $rent);

    /**
     * Create New Data
     * @param  RentParam $rentParam
     * @param  Rent $rent
     * @return Rent|null
     */
    public function create(RentParam $rentParam, Rent $rent): ?Rent;

    /**
     * Update Data By ID
     * @param  int $id
     * @param  RentParam $rentParam
     * @param  Rent $rent
     * @return Rent|null
     */
    public function updateById(int $id, RentParam $rentParam, Rent $rent): ?Rent;

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  Rent $rent
     * @return bool
     */
    public function deleteById(int $id, Rent $rent): bool;
}
