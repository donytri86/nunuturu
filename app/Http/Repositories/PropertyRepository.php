<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contract\PropertyRepositoryContract;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\PropertyParam;
use App\Models\Property;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

/**
 * Repository PropertyRepository
 * @package App\Http\Repositories
 */
class PropertyRepository implements PropertyRepositoryContract
{
    /**
     * Get All Data
     * @param Property $property
     * @return Collection
     */
    public function getAll(Property $property): Collection
    {
        return $property->get();
    }

    /**
     * Get Data By ID
     * @param $id
     * @param Property $property
     * @return Property|null
     */
    public function getById($id, Property $property): ?Property
    {
        return $property->find($id);
    }

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyParam $propertyParam
     * @param Property $property
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, PropertyParam $propertyParam, Property $property): int
    {
        return $property
            ->when($propertyParam->getUserId() != null, function ($q) use ($propertyParam) {
                $q->whereUserId($propertyParam->getUserId());
            })
            ->when($propertyParam->getCity() != null, function ($q) use ($propertyParam) {
                $q->whereCity($propertyParam->getCity());
            })
            ->when($propertyParam->getState() != null, function ($q) use ($propertyParam) {
                $q->whereState($propertyParam->getState());
            })
            ->count();
    }

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyParam $propertyParam
     * @param Property $property
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, PropertyParam $propertyParam, Property $property): int
    {
        $property = $this->getGeneralSearch($generalSearchParam, $propertyParam, $property);

        return $property->count();
    }

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyParam $propertyParam
     * @param Property $property
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, PropertyParam $propertyParam, Property $property): Collection
    {
        $data = $this->getGeneralSearch($generalSearchParam, $propertyParam, $property);

        $data = $data
            ->limit($generalSearchParam->getLength())
            ->offset($generalSearchParam->getStart())
            ->orderBy($generalSearchParam->getOrderColumn(), $generalSearchParam->getOrderDirection())
            ->get();

        return $data;
    }

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyParam $propertyParam
     * @param Property $property
     * @return Property
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, PropertyParam $propertyParam, Property $property)
    {
        $property = $property
            ->with([])
            ->when($propertyParam->getUserId() != null, function ($q) use ($propertyParam) {
                $q->whereUserId($propertyParam->getUserId());
            })
            ->when($propertyParam->getCity() != null, function ($q) use ($propertyParam) {
                $q->whereCity($propertyParam->getCity());
            })
            ->when($propertyParam->getState() != null, function ($q) use ($propertyParam) {
                $q->whereState($propertyParam->getState());
            })
            ->when(! empty($generalSearchParam->getQuery()), function ($q) use ($generalSearchParam) {
                $q->where(function ($q) use ($generalSearchParam) {
                    $q
                        ->where('slug', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('title', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('description', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('status_id', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('address_1', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('address_2', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('city', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('state', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('zip_code', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('long', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('lat', 'like', "%{$generalSearchParam->getQuery()}%");
                });
            });

        return $property;
    }

    /**
     * Create New Data
     * @param  PropertyParam $propertyParam
     * @param  Property $property
     * @return Property|null
     */
    public function create(PropertyParam $propertyParam, Property $property): ?Property
    {
        try {
            if (! is_null($propertyParam->getSlug())) {
                $property->slug = $propertyParam->getSlug();
            }

            if (! is_null($propertyParam->getTitle())) {
                $property->title = $propertyParam->getTitle();
            }

            if (! is_null($propertyParam->getDescription())) {
                $property->description = $propertyParam->getDescription();
            }

            if (! is_null($propertyParam->getStatusId())) {
                $property->status_id = $propertyParam->getStatusId();
            }

            if (! is_null($propertyParam->getAddress1())) {
                $property->address_1 = $propertyParam->getAddress1();
            }

            if (! is_null($propertyParam->getAddress2())) {
                $property->address_2 = $propertyParam->getAddress2();
            }

            if (! is_null($propertyParam->getCity())) {
                $property->city = $propertyParam->getCity();
            }

            if (! is_null($propertyParam->getState())) {
                $property->state = $propertyParam->getState();
            }

            if (! is_null($propertyParam->getZipCode())) {
                $property->zip_code = $propertyParam->getZipCode();
            }

            if (! is_null($propertyParam->getLong())) {
                $property->long = $propertyParam->getLong();
            }

            if (! is_null($propertyParam->getLat())) {
                $property->lat = $propertyParam->getLat();
            }

            if (! is_null($propertyParam->getUserId())) {
                $property->user_id = $propertyParam->getUserId();
            }

            if (! is_null($propertyParam->getCreatedBy())) {
                $property->created_by = $propertyParam->getCreatedBy();
            }

            if (! is_null($propertyParam->getUpdatedBy())) {
                $property->updated_by = $propertyParam->getUpdatedBy();
            }

            $property->save();

            return $property;
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Update Data By ID
     * @param  int $id
     * @param  PropertyParam $propertyParam
     * @param  Property $property
     * @return Property|null
     */
    public function updateById(int $id, PropertyParam $propertyParam, Property $property): ?Property
    {
        try {
            $property = $this->getById($id, $property);

            if ($property != null) {
                $property = $this->create($propertyParam, $property);

                return $property;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  Property $property
     * @return bool
     */
    public function deleteById(int $id, Property $property): bool
    {
        try {
            $property = $this->getById($id, $property);

            if ($property != null) {
                $property->delete();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
