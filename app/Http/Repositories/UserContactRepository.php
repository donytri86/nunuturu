<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contract\UserContactRepositoryContract;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\UserContactParam;
use App\Models\UserContact;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

/**
 * Repository UserContactRepository
 * @package App\Http\Repositories
 */
class UserContactRepository implements UserContactRepositoryContract
{
    /**
     * Get All Data
     * @param UserContact $userContact
     * @return Collection
     */
    public function getAll(UserContact $userContact): Collection
    {
        return $userContact->get();
    }

    /**
     * Get Data By ID
     * @param $id
     * @param UserContact $userContact
     * @return UserContact|null
     */
    public function getById($id, UserContact $userContact): ?UserContact
    {
        return $userContact->find($id);
    }

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param UserContact $userContact
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, UserContact $userContact): int
    {
        return $userContact->count();
    }

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param UserContact $userContact
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, UserContact $userContact): int
    {
        $userContact = $this->getGeneralSearch($generalSearchParam, $userContact);

        return $userContact->count();
    }

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param UserContact $userContact
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, UserContact $userContact): Collection
    {
        $data = $this->getGeneralSearch($generalSearchParam, $userContact);

        $data = $data
            ->limit($generalSearchParam->getLength())
            ->offset($generalSearchParam->getStart())
            ->orderBy($generalSearchParam->getOrderColumn(), $generalSearchParam->getOrderDirection())
            ->get();

        return $data;
    }

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param UserContact $userContact
     * @return UserContact
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, UserContact $userContact)
    {
        $userContact = $userContact
            ->with([])
            ->when(! empty($generalSearchParam->getQuery()), function ($q) use ($generalSearchParam) {
                $q->where(function ($q) use ($generalSearchParam) {
                    $q
                        ->where('contact', 'like', "%{$generalSearchParam->getQuery()}%");
                });
            });

        return $userContact;
    }

    /**
     * Create New Data
     * @param  UserContactParam $userContactParam
     * @param  UserContact $userContact
     * @return UserContact|null
     */
    public function create(UserContactParam $userContactParam, UserContact $userContact): ?UserContact
    {
        try {
            if (! is_null($userContactParam->getUserId())) {
                $userContact->user_id = $userContactParam->getUserId();
            }

            if (! is_null($userContactParam->getContactId())) {
                $userContact->contact_id = $userContactParam->getContactId();
            }

            if (! is_null($userContactParam->getContact())) {
                $userContact->contact = $userContactParam->getContact();
            }

            if (! is_null($userContactParam->getCreatedBy())) {
                $userContact->created_by = $userContactParam->getCreatedBy();
            }

            if (! is_null($userContactParam->getUpdatedBy())) {
                $userContact->updated_by = $userContactParam->getUpdatedBy();
            }

            $userContact->save();

            return $userContact;
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Update Data By ID
     * @param  int $id
     * @param  UserContactParam $userContactParam
     * @param  UserContact $userContact
     * @return UserContact|null
     */
    public function updateById(int $id, UserContactParam $userContactParam, UserContact $userContact): ?UserContact
    {
        try {
            $userContact = $this->getById($id, $userContact);

            if ($userContact != null) {
                $userContact = $this->create($userContactParam, $userContact);

                return $userContact;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  UserContact $userContact
     * @return bool
     */
    public function deleteById(int $id, UserContact $userContact): bool
    {
        try {
            $userContact = $this->getById($id, $userContact);

            if ($userContact != null) {
                $userContact->delete();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
