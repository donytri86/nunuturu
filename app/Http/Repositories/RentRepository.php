<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contract\RentRepositoryContract;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RentParam;
use App\Models\Rent;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

/**
 * Repository RentRepository
 * @package App\Http\Repositories
 */
class RentRepository implements RentRepositoryContract
{
    /**
     * Get All Data
     * @param Rent $rent
     * @return Collection
     */
    public function getAll(Rent $rent): Collection
    {
        return $rent->get();
    }

    /**
     * Get Data By ID
     * @param $id
     * @param Rent $rent
     * @return Rent|null
     */
    public function getById($id, Rent $rent): ?Rent
    {
        return $rent->find($id);
    }

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param RentParam $rentParam
     * @param Rent $rent
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, RentParam $rentParam, Rent $rent): int
    {
        return $rent
            ->when($rentParam->getUserId() != null, function ($q) use ($rentParam) {
                $q->whereUserId($rentParam->getUserId());
            })
            ->when($rentParam->getPropertyId() != null, function ($q) use ($rentParam) {
                $q->wherePropertyId($rentParam->getPropertyId());
            })
            ->when($rentParam->getPropertyOwnerId() != null, function ($q) use ($rentParam) {
                $q->whereHas('property', function ($q) use ($rentParam) {
                    $q->whereUserId($rentParam->getPropertyOwnerId());
                });
            })
            ->count();
    }

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param RentParam $rentParam
     * @param Rent $rent
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, RentParam $rentParam, Rent $rent): int
    {
        $rent = $this->getGeneralSearch($generalSearchParam, $rentParam, $rent);

        return $rent->count();
    }

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param RentParam $rentParam
     * @param Rent $rent
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, RentParam $rentParam, Rent $rent): Collection
    {
        $data = $this->getGeneralSearch($generalSearchParam, $rentParam, $rent);

        $data = $data
            ->limit($generalSearchParam->getLength())
            ->offset($generalSearchParam->getStart())
            ->orderBy($generalSearchParam->getOrderColumn(), $generalSearchParam->getOrderDirection())
            ->get();

        return $data;
    }

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param RentParam $rentParam
     * @param Rent $rent
     * @return Rent
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, RentParam $rentParam, Rent $rent)
    {
        $rent = $rent
            ->with([])
            ->when($rentParam->getUserId() != null, function ($q) use ($rentParam) {
                $q->whereUserId($rentParam->getUserId());
            })
            ->when($rentParam->getPropertyId() != null, function ($q) use ($rentParam) {
                $q->wherePropertyId($rentParam->getPropertyId());
            })
            ->when($rentParam->getPropertyOwnerId() != null, function ($q) use ($rentParam) {
                $q->whereHas('property', function ($q) use ($rentParam) {
                    $q->whereUserId($rentParam->getPropertyOwnerId());
                });
            })
            ->when(! empty($generalSearchParam->getQuery()), function ($q) use ($generalSearchParam) {
                $q->where(function ($q) use ($generalSearchParam) {
                    $q
                        ->where('rent_date', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('rent_period', 'like', "%{$generalSearchParam->getQuery()}%");
                });
            });

        return $rent;
    }

    /**
     * Create New Data
     * @param  RentParam $rentParam
     * @param  Rent $rent
     * @return Rent|null
     */
    public function create(RentParam $rentParam, Rent $rent): ?Rent
    {
        try {
            if (! is_null($rentParam->getPropertyId())) {
                $rent->property_id = $rentParam->getPropertyId();
            }

            if (! is_null($rentParam->getUserId())) {
                $rent->user_id = $rentParam->getUserId();
            }

            if (! is_null($rentParam->getRentDate())) {
                $rent->rent_date = $rentParam->getRentDate();
            }

            if (! is_null($rentParam->getRentPeriod())) {
                $rent->rent_period = $rentParam->getRentPeriod();
            }

            if (! is_null($rentParam->getPeriodId())) {
                $rent->period_id = $rentParam->getPeriodId();
            }

            if (! is_null($rentParam->getPrice())) {
                $rent->price = $rentParam->getPrice();
            }

            $rent->save();

            return $rent;
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Update Data By ID
     * @param  int $id
     * @param  RentParam $rentParam
     * @param  Rent $rent
     * @return Rent|null
     */
    public function updateById(int $id, RentParam $rentParam, Rent $rent): ?Rent
    {
        try {
            $rent = $this->getById($id, $rent);

            if ($rent != null) {
                $rent = $this->create($rentParam, $rent);

                return $rent;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  Rent $rent
     * @return bool
     */
    public function deleteById(int $id, Rent $rent): bool
    {
        try {
            $rent = $this->getById($id, $rent);

            if ($rent != null) {
                $rent->delete();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
