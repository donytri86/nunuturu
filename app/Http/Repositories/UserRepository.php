<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contract\UserRepositoryContract;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\UserParam;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

/**
 * Repository UserRepository
 * @package App\Http\Repositories
 */
class UserRepository implements UserRepositoryContract
{
    /**
     * Get All Data
     * @param User $user
     * @return Collection
     */
    public function getAll(User $user): Collection
    {
        return $user->get();
    }

    /**
     * Get Data By ID
     * @param $id
     * @param User $user
     * @return User|null
     */
    public function getById($id, User $user): ?User
    {
        return $user->find($id);
    }

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param User $user
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, User $user): int
    {
        return $user->count();
    }

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param User $user
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, User $user): int
    {
        $user = $this->getGeneralSearch($generalSearchParam, $user);

        return $user->count();
    }

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param User $user
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, User $user): Collection
    {
        $data = $this->getGeneralSearch($generalSearchParam, $user);

        $data = $data
            ->limit($generalSearchParam->getLength())
            ->offset($generalSearchParam->getStart())
            ->orderBy($generalSearchParam->getOrderColumn(), $generalSearchParam->getOrderDirection())
            ->get();

        return $data;
    }

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param User $user
     * @return User
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, User $user)
    {
        $user = $user
            ->with([])
            ->when(! empty($generalSearchParam->getQuery()), function ($q) use ($generalSearchParam) {
                $q->where(function ($q) use ($generalSearchParam) {
                    $q
                        ->where('first_name', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('last_name', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('email', 'like', "%{$generalSearchParam->getQuery()}%");
                });
            });

        return $user;
    }

    /**
     * Create New Data
     * @param  UserParam $userParam
     * @param  User $user
     * @return User|null
     */
    public function create(UserParam $userParam, User $user): ?User
    {
        try {
            if (! is_null($userParam->getFirstName())) {
                $user->first_name = $userParam->getFirstName();
            }

            if (! is_null($userParam->getLastName())) {
                $user->last_name = $userParam->getLastName();
            }

            if (! is_null($userParam->getEmail())) {
                $user->email = $userParam->getEmail();
            }

            if (! is_null($userParam->getStatusId())) {
                $user->status_id = $userParam->getStatusId();
            }

            if (! is_null($userParam->getEmailVerifiedAt())) {
                $user->email_verified_at = $userParam->getEmailVerifiedAt();
            }

            if (! is_null($userParam->getPassword())) {
                $user->password = bcrypt($userParam->getPassword());
            }

            $user->save();

            return $user;
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Update Data By ID
     * @param  int $id
     * @param  UserParam $userParam
     * @param  User $user
     * @return User|null
     */
    public function updateById(int $id, UserParam $userParam, User $user): ?User
    {
        try {
            $user = $this->getById($id, $user);

            if ($user != null) {
                $user = $this->create($userParam, $user);

                return $user;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  User $user
     * @return bool
     */
    public function deleteById(int $id, User $user): bool
    {
        try {
            $user = $this->getById($id, $user);

            if ($user != null) {
                $user->delete();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
