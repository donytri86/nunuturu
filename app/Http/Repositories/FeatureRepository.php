<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contract\FeatureRepositoryContract;
use App\Http\Params\FeatureParam;
use App\Http\Params\GeneralSearchParam;
use App\Models\Feature;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

/**
 * Repository FeatureRepository
 * @package App\Http\Repositories
 */
class FeatureRepository implements FeatureRepositoryContract
{
    /**
     * Get All Data
     * @param Feature $feature
     * @return Collection
     */
    public function getAll(Feature $feature): Collection
    {
        return $feature->get();
    }

    /**
     * Get Data By ID
     * @param $id
     * @param Feature $feature
     * @return Feature|null
     */
    public function getById($id, Feature $feature): ?Feature
    {
        return $feature->find($id);
    }

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param Feature $feature
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, Feature $feature): int
    {
        return $feature->count();
    }

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param Feature $feature
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, Feature $feature): int
    {
        $feature = $this->getGeneralSearch($generalSearchParam, $feature);

        return $feature->count();
    }

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param Feature $feature
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, Feature $feature): Collection
    {
        $data = $this->getGeneralSearch($generalSearchParam, $feature);

        $data = $data
            ->limit($generalSearchParam->getLength())
            ->offset($generalSearchParam->getStart())
            ->orderBy($generalSearchParam->getOrderColumn(), $generalSearchParam->getOrderDirection())
            ->get();

        return $data;
    }

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param Feature $feature
     * @return Feature
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, Feature $feature)
    {
        $feature = $feature
            ->with([])
            ->when(! empty($generalSearchParam->getQuery()), function ($q) use ($generalSearchParam) {
                $q->where(function ($q) use ($generalSearchParam) {
                    $q
                        ->where('slug', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('name', 'like', "%{$generalSearchParam->getQuery()}%");
                });
            });

        return $feature;
    }

    /**
     * Create New Data
     * @param  FeatureParam $featureParam
     * @param  Feature $feature
     * @return Feature|null
     */
    public function create(FeatureParam $featureParam, Feature $feature): ?Feature
    {
        try {
            if (! is_null($featureParam->getSlug())) {
                $feature->slug = $featureParam->getSlug();
            }

            if (! is_null($featureParam->getName())) {
                $feature->name = $featureParam->getName();
            }

            if (! is_null($featureParam->getCreatedBy())) {
                $feature->created_by = $featureParam->getCreatedBy();
            }

            if (! is_null($featureParam->getUpdatedBy())) {
                $feature->updated_by = $featureParam->getUpdatedBy();
            }

            $feature->save();

            return $feature;
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Update Data By ID
     * @param  int $id
     * @param  FeatureParam $featureParam
     * @param  Feature $feature
     * @return Feature|null
     */
    public function updateById(int $id, FeatureParam $featureParam, Feature $feature): ?Feature
    {
        try {
            $feature = $this->getById($id, $feature);

            if ($feature != null) {
                $feature = $this->create($featureParam, $feature);

                return $feature;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  Feature $feature
     * @return bool
     */
    public function deleteById(int $id, Feature $feature): bool
    {
        try {
            $feature = $this->getById($id, $feature);

            if ($feature != null) {
                $feature->delete();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
