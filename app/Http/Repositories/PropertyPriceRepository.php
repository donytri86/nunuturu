<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contract\PropertyPriceRepositoryContract;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\PropertyPriceParam;
use App\Models\PropertyPrice;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

/**
 * Repository PropertyPriceRepository
 * @package App\Http\Repositories
 */
class PropertyPriceRepository implements PropertyPriceRepositoryContract
{
    /**
     * Get All Data
     * @param PropertyPrice $propertyPrice
     * @return Collection
     */
    public function getAll(PropertyPrice $propertyPrice): Collection
    {
        return $propertyPrice->get();
    }

    /**
     * Get Data By ID
     * @param $id
     * @param PropertyPrice $propertyPrice
     * @return PropertyPrice|null
     */
    public function getById($id, PropertyPrice $propertyPrice): ?PropertyPrice
    {
        return $propertyPrice->find($id);
    }

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyPrice $propertyPrice
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, PropertyPrice $propertyPrice): int
    {
        return $propertyPrice->count();
    }

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyPrice $propertyPrice
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, PropertyPrice $propertyPrice): int
    {
        $propertyPrice = $this->getGeneralSearch($generalSearchParam, $propertyPrice);

        return $propertyPrice->count();
    }

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyPrice $propertyPrice
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, PropertyPrice $propertyPrice): Collection
    {
        $data = $this->getGeneralSearch($generalSearchParam, $propertyPrice);

        $data = $data
            ->limit($generalSearchParam->getLength())
            ->offset($generalSearchParam->getStart())
            ->orderBy($generalSearchParam->getOrderColumn(), $generalSearchParam->getOrderDirection())
            ->get();

        return $data;
    }

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param PropertyPrice $propertyPrice
     * @return PropertyPrice
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, PropertyPrice $propertyPrice)
    {
        $propertyPrice = $propertyPrice
            ->with([])
            ->when(! empty($generalSearchParam->getQuery()), function ($q) use ($generalSearchParam) {
                $q->where(function ($q) use ($generalSearchParam) {
                    $q
                        ->where('price', 'like', "%{$generalSearchParam->getQuery()}%");
                });
            });

        return $propertyPrice;
    }

    /**
     * Create New Data
     * @param  PropertyPriceParam $propertyPriceParam
     * @param  PropertyPrice $propertyPrice
     * @return PropertyPrice|null
     */
    public function create(PropertyPriceParam $propertyPriceParam, PropertyPrice $propertyPrice): ?PropertyPrice
    {
        try {
            if (! is_null($propertyPriceParam->getPropertyId())) {
                $propertyPrice->property_id = $propertyPriceParam->getPropertyId();
            }

            if (! is_null($propertyPriceParam->getPeriodId())) {
                $propertyPrice->period_id = $propertyPriceParam->getPeriodId();
            }

            if (! is_null($propertyPriceParam->getPrice())) {
                $propertyPrice->price = $propertyPriceParam->getPrice();
            }

            $propertyPrice->save();

            return $propertyPrice;
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Update Data By ID
     * @param  int $id
     * @param  PropertyPriceParam $propertyPriceParam
     * @param  PropertyPrice $propertyPrice
     * @return PropertyPrice|null
     */
    public function updateById(int $id, PropertyPriceParam $propertyPriceParam, PropertyPrice $propertyPrice): ?PropertyPrice
    {
        try {
            $propertyPrice = $this->getById($id, $propertyPrice);

            if ($propertyPrice != null) {
                $propertyPrice = $this->create($propertyPriceParam, $propertyPrice);

                return $propertyPrice;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  PropertyPrice $propertyPrice
     * @return bool
     */
    public function deleteById(int $id, PropertyPrice $propertyPrice): bool
    {
        try {
            $propertyPrice = $this->getById($id, $propertyPrice);

            if ($propertyPrice != null) {
                $propertyPrice->delete();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
