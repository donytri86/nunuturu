<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contract\FileRepositoryContract;
use App\Http\Params\FileParam;
use App\Http\Params\GeneralSearchParam;
use App\Models\File;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

/**
 * Repository FileRepository
 * @package App\Http\Repositories
 */
class FileRepository implements FileRepositoryContract
{
    /**
     * Get All Data
     * @param File $file
     * @return Collection
     */
    public function getAll(File $file): Collection
    {
        return $file->get();
    }

    /**
     * Get Data By ID
     * @param $id
     * @param File $file
     * @return File|null
     */
    public function getById($id, File $file): ?File
    {
        return $file->find($id);
    }

    /**
     * Get Data By Filename
     * @param $filename
     * @param File $file
     * @return File|null
     */
    public function getByFilename($filename, File $file): ?File
    {
        return $file->whereFilename($filename);
    }

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param File $file
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, File $file): int
    {
        return $file->count();
    }

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param File $file
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, File $file): int
    {
        $file = $this->getGeneralSearch($generalSearchParam, $file);

        return $file->count();
    }

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param File $file
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, File $file): Collection
    {
        $data = $this->getGeneralSearch($generalSearchParam, $file);

        $data = $data
            ->limit($generalSearchParam->getLength())
            ->offset($generalSearchParam->getStart())
            ->orderBy($generalSearchParam->getOrderColumn(), $generalSearchParam->getOrderDirection())
            ->get();

        return $data;
    }

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param File $file
     * @return File
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, File $file)
    {
        $file = $file
            ->with([])
            ->when(! empty($generalSearchParam->getQuery()), function ($q) use ($generalSearchParam) {
                $q->where(function ($q) use ($generalSearchParam) {
                    $q
                        ->where('filename', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('path', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('thumbnail_path', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('mime_type', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('description', 'like', "%{$generalSearchParam->getQuery()}%");
                });
            });

        return $file;
    }

    /**
     * Create New Data
     * @param  FileParam $fileParam
     * @param  File $file
     * @return File|null
     */
    public function create(FileParam $fileParam, File $file): ?File
    {
        try {
            if (! is_null($fileParam->getFilename())) {
                $file->filename = $fileParam->getFilename();
            }

            if (! is_null($fileParam->getPath())) {
                $file->path = $fileParam->getPath();
            }

            if (! is_null($fileParam->getThumbnailPath())) {
                $file->thumbnail_path = $fileParam->getThumbnailPath();
            }

            if (! is_null($fileParam->getMimeType())) {
                $file->mime_type = $fileParam->getMimeType();
            }

            if (! is_null($fileParam->getVisibilityId())) {
                $file->visibility_id = $fileParam->getVisibilityId();
            }

            if (! is_null($fileParam->getDescription())) {
                $file->description = $fileParam->getDescription();
            }

            if (! is_null($fileParam->getUserId())) {
                $file->user_id = $fileParam->getUserId();
            }

            if (! is_null($fileParam->getCreatedBy())) {
                $file->created_by = $fileParam->getCreatedBy();
            }

            if (! is_null($fileParam->getUpdatedBy())) {
                $file->updated_by = $fileParam->getUpdatedBy();
            }

            $file->save();

            return $file;
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Update Data By ID
     * @param  int $id
     * @param  FileParam $fileParam
     * @param  File $file
     * @return File|null
     */
    public function updateById(int $id, FileParam $fileParam, File $file): ?File
    {
        try {
            $file = $this->getById($id, $file);

            if ($file != null) {
                $file = $this->create($fileParam, $file);

                return $file;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  File $file
     * @return bool
     */
    public function deleteById(int $id, File $file): bool
    {
        try {
            $file = $this->getById($id, $file);

            if ($file != null) {
                $file->delete();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
