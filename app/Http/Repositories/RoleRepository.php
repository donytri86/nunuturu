<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contract\RoleRepositoryContract;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RoleParam;
use App\Models\Role;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

/**
 * Repository RoleRepository
 * @package App\Http\Repositories
 */
class RoleRepository implements RoleRepositoryContract
{
    /**
     * Get All Data
     * @param Role $role
     * @return Collection
     */
    public function getAll(Role $role): Collection
    {
        return $role->get();
    }

    /**
     * Get Data By ID
     * @param $id
     * @param Role $role
     * @return Role|null
     */
    public function getById($id, Role $role): ?Role
    {
        return $role->find($id);
    }

    /**
     * Get Records Total
     * @param GeneralSearchParam $generalSearchParam
     * @param Role $role
     * @return int
     */
    public function getRecordsTotal(GeneralSearchParam $generalSearchParam, Role $role): int
    {
        return $role->count();
    }

    /**
     * Get Records Filtered
     * @param GeneralSearchParam $generalSearchParam
     * @param Role $role
     * @return int
     */
    public function getRecordsFiltered(GeneralSearchParam $generalSearchParam, Role $role): int
    {
        $role = $this->getGeneralSearch($generalSearchParam, $role);

        return $role->count();
    }

    /**
     * Get Records Data
     * @param GeneralSearchParam $generalSearchParam
     * @param Role $role
     * @return Collection
     */
    public function getRecordsData(GeneralSearchParam $generalSearchParam, Role $role): Collection
    {
        $data = $this->getGeneralSearch($generalSearchParam, $role);

        $data = $data
            ->limit($generalSearchParam->getLength())
            ->offset($generalSearchParam->getStart())
            ->orderBy($generalSearchParam->getOrderColumn(), $generalSearchParam->getOrderDirection())
            ->get();

        return $data;
    }

    /**
     * Get General Search Query
     * @param GeneralSearchParam $generalSearchParam
     * @param Role $role
     * @return Role
     */
    public function getGeneralSearch(GeneralSearchParam $generalSearchParam, Role $role)
    {
        $role = $role
            ->with([])
            ->when(! empty($generalSearchParam->getQuery()), function ($q) use ($generalSearchParam) {
                $q->where(function ($q) use ($generalSearchParam) {
                    $q
                        ->where('slug', 'like', "%{$generalSearchParam->getQuery()}%")
                        ->orWhere('name', 'like', "%{$generalSearchParam->getQuery()}%");
                });
            });

        return $role;
    }

    /**
     * Create New Data
     * @param  RoleParam $roleParam
     * @param  Role $role
     * @return Role|null
     */
    public function create(RoleParam $roleParam, Role $role): ?Role
    {
        try {
            if (! is_null($roleParam->getSlug())) {
                $role->slug = $roleParam->getSlug();
            }

            if (! is_null($roleParam->getName())) {
                $role->name = $roleParam->getName();
            }

            if (! is_null($roleParam->getCreatedBy())) {
                $role->created_by = $roleParam->getCreatedBy();
            }

            if (! is_null($roleParam->getUpdatedBy())) {
                $role->updated_by = $roleParam->getUpdatedBy();
            }

            $role->save();

            return $role;
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Update Data By ID
     * @param  int $id
     * @param  RoleParam $roleParam
     * @param  Role $role
     * @return Role|null
     */
    public function updateById(int $id, RoleParam $roleParam, Role $role): ?Role
    {
        try {
            $role = $this->getById($id, $role);

            if ($role != null) {
                $role = $this->create($roleParam, $role);

                return $role;
            } else {
                return null;
            }
        } catch (\Exception $e) {
            report($e);

            return null;
        }
    }

    /**
     * Delete Data By ID
     * @param  int $id
     * @param  Role $role
     * @return bool
     */
    public function deleteById(int $id, Role $role): bool
    {
        try {
            $role = $this->getById($id, $role);

            if ($role != null) {
                $role->delete();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
