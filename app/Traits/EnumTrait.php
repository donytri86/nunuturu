<?php

namespace App\Traits;

use Illuminate\Support\Str;
use App\Exceptions\EnumException;
use stdClass;

trait EnumTrait
{
    public function toArray()
    {
        $array = parent::toArray();

        foreach (self::$enum_casts as $key => $value) {
            $array[$key] = $this->getAttribute($key);
        }

        return $array;
    }

    public function setAttribute($key, $value)
    {
        if (self::isEnum($key)) {
            throw new EnumException("Cannot directly set the enum attributes", 400);
        } elseif (self::isEnumValue($key)) {
            return parent::setAttribute($key, $value['id'] ?? $value);
        } else {
            return parent::setAttribute($key, $value);
        }
    }

    public function getAttribute($key)
    {
        if (self::isEnum($key)) {
            $this->setEnum($key);

            return $this->attributes[$key];
        }

        return parent::getAttribute($key);
    }

    public function setEnum($key)
    {
        if (! isset($this->attributes[$key])) {
            $this->attributes[$key] = new stdClass;
        }

        $enum_value = self::$enum_casts[$key];
        $enum_data = self::$enums[$key][$this->attributes[$enum_value]];
        $this->attributes[$key]->id = $enum_data['id'];
        $this->attributes[$key]->key = $enum_data['key'];
        $this->attributes[$key]->description = $enum_data['description'] ?? $enum_data['key'];

        return $this->attributes[$key];
    }

    public static function isEnum($key)
    {
        return isset(self::$enum_casts[$key]);
    }

    public static function isEnumValue($key)
    {
        return array_search($key, self::$enum_casts);
    }
}
