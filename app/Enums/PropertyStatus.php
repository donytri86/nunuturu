<?php

namespace App\Enums;

interface PropertyStatus
{
    /**
     * PropertyStatus
     * 0 AVAILABLE
     * 1 RENT
     */

    public const AVAILABLE = [
        'id' => 0,
        'key' => 'AVAILABLE',
        'description' => 'Available'
    ];

    public const RENT = [
        'id' => 1,
        'key' => 'RENT',
        'description' => 'Rent by Someone'
    ];
}
