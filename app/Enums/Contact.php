<?php

namespace App\Enums;

interface Contact
{
    /**
     * Contact
     * 0 PHONE
     * 1 FACEBOOK
     * 2 INSTAGRAM
     */

    public const PHONE = [
        'id' => 0,
        'key' => 'PHONE',
        'description' => 'Phone'
    ];

    public const FACEBOOK = [
        'id' => 1,
        'key' => 'FACEBOOK',
        'description' => 'Facebok'
    ];

    public const INSTAGRAM = [
        'id' => 2,
        'key' => 'INSTAGRAM',
        'description' => 'Instagram'
    ];
}
