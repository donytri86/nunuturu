<?php

namespace App\Enums;

interface FileVisibility
{
    /**
     * FileVisibility
     * 0 VISIBLE
     * 1 INVISIBLE
     */

    public const VISIBLE = [
        'id' => 0,
        'key' => 'VISIBLE',
        'description' => 'Visible'
    ];

    public const INVISIBLE = [
        'id' => 0,
        'key' => 'INVISIBLE',
        'description' => 'Invisible'
    ];
}
