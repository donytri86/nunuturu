<?php

namespace App\Enums;

interface Period
{
    /**
     * Period
     * 0 DAILY
     * 1 MONTHLY
     * 2 ANNUALY
     */

    public const DAILY = [
        'id' => 0,
        'key' => 'DAILY',
        'description' => 'Daily'
    ];

    public const MONTHLY = [
        'id' => 1,
        'key' => 'MONTHLY',
        'description' => 'Monthly'
    ];

    public const ANNUALY = [
        'id' => 2,
        'key' => 'ANNUALY',
        'description' => 'Annualy'
    ];
}
