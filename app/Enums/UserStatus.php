<?php

namespace App\Enums;

interface UserStatus
{
    /**
     * UserStatus
     * 0 INACTIVE
     * 1 ACTIVE
     * 2 BANNED
     */

    public const INACTIVE = [
        'id' => 0,
        'key' => 'INACTIVE',
        'description' => 'Inactive'
    ];

    public const ACTIVE = [
        'id' => 1,
        'key' => 'ACTIVE',
        'description' => 'Active'
    ];

    public const BANNED = [
        'id' => 2,
        'key' => 'BANNED',
        'description' => 'Banned'
    ];
}
