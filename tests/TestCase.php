<?php

namespace Tests;

use Faker\Factory as Faker;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    private $faker;
    private $container;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate');
        Artisan::call('db:seed');
    }

    public function tearDown(): void
    {
        // parent::tearDown();
    }

    protected function getFaker()
    {
        if ($this->faker == null) {
            $this->faker = Faker::create('id_ID');
        }

        return $this->faker;
    }

    protected function getContainer(): Container
    {
        if ($this->container == null) {
            $this->container = new Container;
        }

        return $this->container;
    }
}
