<?php

namespace Tests\Unit\Repositories;

use App\Enums\Contact;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\UserContactParam;
use App\Http\Repositories\Contract\UserContactRepositoryContract as UserContactRepository;
use App\Models\UserContact;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserContactRepositoryTest extends TestCase
{
    private $userContactRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->userContactRepository = $this->app->make(UserContactRepository::class);
    }

    public function dummy()
    {
        $userContactParam = $this->app->make(UserContactParam::class);

        $name = $this->getFaker()->name();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $userContactParam->setUserId($user->id);
        $userContactParam->setContactId(Contact::PHONE['id']);
        $userContactParam->setContact($this->getFaker()->phoneNumber());
        $userContactParam->setCreatedBy($user->id);
        $userContactParam->setUpdatedBy($user->id);

        return $userContactParam;
    }

    public function dummyDatatable()
    {
        $generalSearchParam = $this->app->make(GeneralSearchParam::class);

        $generalSearchParam->setQuery('');
        $generalSearchParam->setLength(10);
        $generalSearchParam->setStart(0);
        $generalSearchParam->setOrderColumn('created_at');
        $generalSearchParam->setOrderDirection('desc');

        return $generalSearchParam;
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $userContact = $this->getContainer()->call(
            [$this->userContactRepository, 'create'],
            [
                'userContactParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getUserId(), $userContact->user_id);
        $this->assertEquals($dummy->getContactId(), $userContact->contact_id);
        $this->assertEquals($dummy->getContact(), $userContact->contact);
        $this->assertEquals($dummy->getCreatedBy(), $userContact->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $userContact->updated_by);
        $this->assertNotEquals(null, $userContact);
    }

    public function testUpdateById()
    {
        $dummy = $this->dummy();

        $get_user_contact = UserContact::inRandomOrder()->first();
        if (! $get_user_contact) {
            $get_user_contact = UserContact::factory()->create();
        }

        $userContact = $this->getContainer()->call(
            [$this->userContactRepository, 'updateById'],
            [
                'id' => $get_user_contact->id,
                'userContactParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getUserId(), $userContact->user_id);
        $this->assertEquals($dummy->getContactId(), $userContact->contact_id);
        $this->assertEquals($dummy->getContact(), $userContact->contact);
        $this->assertEquals($dummy->getCreatedBy(), $userContact->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $userContact->updated_by);
        $this->assertNotEquals(null, $userContact);
    }

    public function testDeleteById()
    {
        $get_user_contact = UserContact::factory()->create();

        $userContact = $this->getContainer()->call(
            [$this->userContactRepository, 'deleteById'],
            [
                'id' => $get_user_contact->id
            ]
        );

        $this->assertTrue($userContact);
    }

    public function testGetAll()
    {
        $userContacts = $this->getContainer()->call(
            [$this->userContactRepository, 'getAll'],
            [

            ]
        );

        $this->assertInstanceOf(Collection::class, $userContacts);
    }

    public function testGetById()
    {
        $get_user_contact = UserContact::inRandomOrder()->first();
        if (! $get_user_contact) {
            $get_user_contact = UserContact::factory()->create();
        }

        $userContact = $this->getContainer()->call(
            [$this->userContactRepository, 'getById'],
            [
                'id' => $get_user_contact->id
            ]
        );

        $this->assertNotEquals(null, $userContact);
        $this->assertEquals($get_user_contact->user_id, $userContact->user_id);
        $this->assertEquals($get_user_contact->contact_id, $userContact->contact_id);
        $this->assertEquals($get_user_contact->contact, $userContact->contact);
        $this->assertEquals($get_user_contact->created_by, $userContact->created_by);
        $this->assertEquals($get_user_contact->updated_by, $userContact->updated_by);
    }

    public function testDatatable()
    {
        $dummy_datatable = $this->dummyDatatable();

        $records_total = $this->getContainer()->call(
            [$this->userContactRepository, 'getRecordsTotal'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_filtered = $this->getContainer()->call(
            [$this->userContactRepository, 'getRecordsFiltered'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_data = $this->getContainer()->call(
            [$this->userContactRepository, 'getRecordsData'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $this->assertInstanceOf(Collection::class, $records_data);
        $this->assertIsNumeric($records_total);
        $this->assertIsNumeric($records_filtered);
    }
}
