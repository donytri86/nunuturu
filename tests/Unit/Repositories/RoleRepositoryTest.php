<?php

namespace Tests\Unit\Repositories;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RoleParam;
use App\Http\Repositories\Contract\RoleRepositoryContract as RoleRepository;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class RoleRepositoryTest extends TestCase
{
    private $roleRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->roleRepository = $this->app->make(RoleRepository::class);
    }

    public function dummy()
    {
        $roleParam = $this->app->make(RoleParam::class);

        $name = $this->getFaker()->name();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $roleParam->setSlug(Str::slug($name));
        $roleParam->setName($name);
        $roleParam->setCreatedBy($user->id);
        $roleParam->setUpdatedBy($user->id);

        return $roleParam;
    }

    public function dummyDatatable()
    {
        $generalSearchParam = $this->app->make(GeneralSearchParam::class);

        $generalSearchParam->setQuery('');
        $generalSearchParam->setLength(10);
        $generalSearchParam->setStart(0);
        $generalSearchParam->setOrderColumn('created_at');
        $generalSearchParam->setOrderDirection('desc');

        return $generalSearchParam;
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $role = $this->getContainer()->call(
            [$this->roleRepository, 'create'],
            [
                'roleParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getSlug(), $role->slug);
        $this->assertEquals($dummy->getName(), $role->name);
        $this->assertEquals($dummy->getCreatedBy(), $role->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $role->updated_by);
        $this->assertNotEquals(null, $role);
    }

    public function testUpdateById()
    {
        $dummy = $this->dummy();

        $get_role = Role::inRandomOrder()->first();
        if (! $get_role) {
            $get_role = Role::factory()->create();
        }

        $role = $this->getContainer()->call(
            [$this->roleRepository, 'updateById'],
            [
                'id' => $get_role->id,
                'roleParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getSlug(), $role->slug);
        $this->assertEquals($dummy->getName(), $role->name);
        $this->assertEquals($dummy->getCreatedBy(), $role->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $role->updated_by);
        $this->assertNotEquals(null, $role);
    }

    public function testDeleteById()
    {
        $get_role = Role::factory()->create();

        $role = $this->getContainer()->call(
            [$this->roleRepository, 'deleteById'],
            [
                'id' => $get_role->id
            ]
        );

        $this->assertTrue($role);
    }

    public function testGetAll()
    {
        $roles = $this->getContainer()->call(
            [$this->roleRepository, 'getAll'],
            [

            ]
        );

        $this->assertInstanceOf(Collection::class, $roles);
    }

    public function testGetById()
    {
        $get_role = Role::inRandomOrder()->first();
        if (! $get_role) {
            $get_role = Role::factory()->create();
        }

        $role = $this->getContainer()->call(
            [$this->roleRepository, 'getById'],
            [
                'id' => $get_role->id
            ]
        );

        $this->assertNotEquals(null, $role);
        $this->assertEquals($get_role->slug, $role->slug);
        $this->assertEquals($get_role->name, $role->name);
        $this->assertEquals($get_role->created_by, $role->created_by);
        $this->assertEquals($get_role->updated_by, $role->updated_by);
    }

    public function testDatatable()
    {
        $dummy_datatable = $this->dummyDatatable();

        $records_total = $this->getContainer()->call(
            [$this->roleRepository, 'getRecordsTotal'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_filtered = $this->getContainer()->call(
            [$this->roleRepository, 'getRecordsFiltered'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_data = $this->getContainer()->call(
            [$this->roleRepository, 'getRecordsData'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $this->assertInstanceOf(Collection::class, $records_data);
        $this->assertIsNumeric($records_total);
        $this->assertIsNumeric($records_filtered);
    }
}
