<?php

namespace Tests\Unit\Repositories;

use App\Enums\Period;
use App\Enums\RentVisibility;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\RentParam;
use App\Http\Repositories\Contract\RentRepositoryContract as RentRepository;
use App\Models\Property;
use App\Models\Rent;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class RentRepositoryTest extends TestCase
{
    private $rentRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->rentRepository = $this->app->make(RentRepository::class);
    }

    public function dummy()
    {
        $rentParam = $this->app->make(RentParam::class);

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $property = Property::inRandomOrder()->first();
        if (! $property) {
            $property = User::factory()->create();
        }

        $rentParam->setPropertyId($property->id);
        $rentParam->setUserId($user->id);
        $rentParam->setRentDate(now());
        $rentParam->setRentPeriod(Period::MONTHLY['id']);
        $rentParam->setPeriodId($this->getFaker()->numberBetween(1, 6));
        $rentParam->setPrice($this->getFaker()->numberBetween(500, 3000));

        return $rentParam;
    }

    public function dummyDatatable()
    {
        $generalSearchParam = $this->app->make(GeneralSearchParam::class);

        $generalSearchParam->setQuery('');
        $generalSearchParam->setLength(10);
        $generalSearchParam->setStart(0);
        $generalSearchParam->setOrderColumn('created_at');
        $generalSearchParam->setOrderDirection('desc');

        return $generalSearchParam;
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $rent = $this->getContainer()->call(
            [$this->rentRepository, 'create'],
            [
                'rentParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getPropertyId(), $rent->property_id);
        $this->assertEquals($dummy->getUserId(), $rent->user_id);
        $this->assertEquals($dummy->getRentDate(), $rent->rent_date);
        $this->assertEquals($dummy->getRentPeriod(), $rent->rent_period);
        $this->assertEquals($dummy->getPeriodId(), $rent->period_id);
        $this->assertEquals($dummy->getPrice(), $rent->price);
        $this->assertNotEquals(null, $rent);
    }

    public function testUpdateById()
    {
        $dummy = $this->dummy();

        $get_rent = Rent::inRandomOrder()->first();
        if (! $get_rent) {
            $get_rent = Rent::factory()->create();
        }

        $rent = $this->getContainer()->call(
            [$this->rentRepository, 'updateById'],
            [
                'id' => $get_rent->id,
                'rentParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getPropertyId(), $rent->property_id);
        $this->assertEquals($dummy->getUserId(), $rent->user_id);
        $this->assertEquals($dummy->getRentDate(), $rent->rent_date);
        $this->assertEquals($dummy->getRentPeriod(), $rent->rent_period);
        $this->assertEquals($dummy->getPeriodId(), $rent->period_id);
        $this->assertEquals($dummy->getPrice(), $rent->price);
        $this->assertNotEquals(null, $rent);
    }

    public function testDeleteById()
    {
        $get_rent = Rent::factory()->create();

        $rent = $this->getContainer()->call(
            [$this->rentRepository, 'deleteById'],
            [
                'id' => $get_rent->id
            ]
        );

        $this->assertTrue($rent);
    }

    public function testGetAll()
    {
        $rents = $this->getContainer()->call(
            [$this->rentRepository, 'getAll'],
            [

            ]
        );

        $this->assertInstanceOf(Collection::class, $rents);
    }

    public function testGetById()
    {
        $get_rent = Rent::inRandomOrder()->first();
        if (! $get_rent) {
            $get_rent = Rent::factory()->create();
        }

        $rent = $this->getContainer()->call(
            [$this->rentRepository, 'getById'],
            [
                'id' => $get_rent->id
            ]
        );

        $this->assertNotEquals(null, $rent);
        $this->assertEquals($get_rent->property_id, $rent->property_id);
        $this->assertEquals($get_rent->user_id, $rent->user_id);
        $this->assertEquals($get_rent->rent_date, $rent->rent_date);
        $this->assertEquals($get_rent->rent_period, $rent->rent_period);
        $this->assertEquals($get_rent->period_id, $rent->period_id);
        $this->assertEquals($get_rent->price, $rent->price);
    }

    public function testDatatable()
    {
        $dummy_datatable = $this->dummyDatatable();

        $records_total = $this->getContainer()->call(
            [$this->rentRepository, 'getRecordsTotal'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_filtered = $this->getContainer()->call(
            [$this->rentRepository, 'getRecordsFiltered'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_data = $this->getContainer()->call(
            [$this->rentRepository, 'getRecordsData'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $this->assertInstanceOf(Collection::class, $records_data);
        $this->assertIsNumeric($records_total);
        $this->assertIsNumeric($records_filtered);
    }
}
