<?php

namespace Tests\Unit\Repositories;

use App\Enums\Period;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\PropertyPriceParam;
use App\Http\Repositories\Contract\PropertyPriceRepositoryContract as PropertyPriceRepository;
use App\Models\Property;
use App\Models\PropertyPrice;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class PropertyPriceRepositoryTest extends TestCase
{
    private $propertyPriceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->propertyPriceRepository = $this->app->make(PropertyPriceRepository::class);
    }

    public function dummy()
    {
        $propertyPriceParam = $this->app->make(PropertyPriceParam::class);

        $name = $this->getFaker()->name();

        $property = Property::inRandomOrder()->first();
        if (! $property) {
            $property = Property::factory()->create();
        }

        $propertyPriceParam->setPropertyId($property->id);
        $propertyPriceParam->setPeriodId(Period::MONTHLY['id']);
        $propertyPriceParam->setPrice($this->getFaker()->numberBetween(500, 3000));

        return $propertyPriceParam;
    }

    public function dummyDatatable()
    {
        $generalSearchParam = $this->app->make(GeneralSearchParam::class);

        $generalSearchParam->setQuery('');
        $generalSearchParam->setLength(10);
        $generalSearchParam->setStart(0);
        $generalSearchParam->setOrderColumn('created_at');
        $generalSearchParam->setOrderDirection('desc');

        return $generalSearchParam;
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $propertyPrice = $this->getContainer()->call(
            [$this->propertyPriceRepository, 'create'],
            [
                'propertyPriceParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getPropertyId(), $propertyPrice->property_id);
        $this->assertEquals($dummy->getPeriodId(), $propertyPrice->period_id);
        $this->assertEquals($dummy->getPrice(), $propertyPrice->price);
        $this->assertNotEquals(null, $propertyPrice);
    }

    public function testUpdateById()
    {
        $dummy = $this->dummy();

        $get_property_price = PropertyPrice::inRandomOrder()->first();
        if (! $get_property_price) {
            $get_property_price = PropertyPrice::factory()->create();
        }

        $propertyPrice = $this->getContainer()->call(
            [$this->propertyPriceRepository, 'updateById'],
            [
                'id' => $get_property_price->id,
                'propertyPriceParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getPropertyId(), $propertyPrice->property_id);
        $this->assertEquals($dummy->getPeriodId(), $propertyPrice->period_id);
        $this->assertEquals($dummy->getPrice(), $propertyPrice->price);
        $this->assertNotEquals(null, $propertyPrice);
    }

    public function testDeleteById()
    {
        $get_property_price = PropertyPrice::factory()->create();

        $propertyPrice = $this->getContainer()->call(
            [$this->propertyPriceRepository, 'deleteById'],
            [
                'id' => $get_property_price->id
            ]
        );

        $this->assertTrue($propertyPrice);
    }

    public function testGetAll()
    {
        $propertyPrices = $this->getContainer()->call(
            [$this->propertyPriceRepository, 'getAll'],
            [

            ]
        );

        $this->assertInstanceOf(Collection::class, $propertyPrices);
    }

    public function testGetById()
    {
        $get_property_price = PropertyPrice::inRandomOrder()->first();
        if (! $get_property_price) {
            $get_property_price = PropertyPrice::factory()->create();
        }

        $propertyPrice = $this->getContainer()->call(
            [$this->propertyPriceRepository, 'getById'],
            [
                'id' => $get_property_price->id
            ]
        );

        $this->assertNotEquals(null, $propertyPrice);
        $this->assertEquals($get_property_price->property_id, $propertyPrice->property_id);
        $this->assertEquals($get_property_price->period_id, $propertyPrice->period_id);
        $this->assertEquals($get_property_price->price, $propertyPrice->price);
    }

    public function testDatatable()
    {
        $dummy_datatable = $this->dummyDatatable();

        $records_total = $this->getContainer()->call(
            [$this->propertyPriceRepository, 'getRecordsTotal'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_filtered = $this->getContainer()->call(
            [$this->propertyPriceRepository, 'getRecordsFiltered'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_data = $this->getContainer()->call(
            [$this->propertyPriceRepository, 'getRecordsData'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $this->assertInstanceOf(Collection::class, $records_data);
        $this->assertIsNumeric($records_total);
        $this->assertIsNumeric($records_filtered);
    }
}
