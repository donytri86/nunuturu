<?php

namespace Tests\Unit\Repositories;

use App\Enums\PropertyStatus;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\PropertyParam;
use App\Http\Repositories\Contract\PropertyRepositoryContract as PropertyRepository;
use App\Models\Property;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class PropertyRepositoryTest extends TestCase
{
    private $propertyRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->propertyRepository = $this->app->make(PropertyRepository::class);
    }

    public function dummy()
    {
        $propertyParam = $this->app->make(PropertyParam::class);

        $title = $this->getFaker()->company();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $propertyParam->setSlug(Str::slug($title));
        $propertyParam->setTitle($title);
        $propertyParam->setDescription($this->getFaker()->sentence(10));
        $propertyParam->setStatusId(PropertyStatus::AVAILABLE['id']);
        $propertyParam->setAddress1($this->getFaker()->streetAddress());
        $propertyParam->setAddress2($this->getFaker()->address());
        $propertyParam->setCity($this->getFaker()->city());
        $propertyParam->setState($this->getFaker()->state());
        $propertyParam->setZipCode($this->getFaker()->postcode());
        $propertyParam->setLong($this->getFaker()->longitude());
        $propertyParam->setLat($this->getFaker()->latitude());
        $propertyParam->setUserId($user->id);
        $propertyParam->setCreatedBy($user->id);
        $propertyParam->setUpdatedBy($user->id);

        return $propertyParam;
    }

    public function dummyDatatable()
    {
        $generalSearchParam = $this->app->make(GeneralSearchParam::class);

        $generalSearchParam->setQuery('');
        $generalSearchParam->setLength(10);
        $generalSearchParam->setStart(0);
        $generalSearchParam->setOrderColumn('created_at');
        $generalSearchParam->setOrderDirection('desc');

        return $generalSearchParam;
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $property = $this->getContainer()->call(
            [$this->propertyRepository, 'create'],
            [
                'propertyParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getSlug(), $property->slug);
        $this->assertEquals($dummy->getTitle(), $property->title);
        $this->assertEquals($dummy->getDescription(), $property->description);
        $this->assertEquals($dummy->getStatusId(), $property->status_id);
        $this->assertEquals($dummy->getAddress1(), $property->address_1);
        $this->assertEquals($dummy->getAddress2(), $property->address_2);
        $this->assertEquals($dummy->getCity(), $property->city);
        $this->assertEquals($dummy->getState(), $property->state);
        $this->assertEquals($dummy->getZipCode(), $property->zip_code);
        $this->assertEquals($dummy->getLong(), $property->long);
        $this->assertEquals($dummy->getLat(), $property->lat);
        $this->assertEquals($dummy->getUserId(), $property->user_id);
        $this->assertEquals($dummy->getCreatedBy(), $property->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $property->updated_by);
        $this->assertNotEquals(null, $property);
    }

    public function testUpdateById()
    {
        $dummy = $this->dummy();

        $get_property = Property::inRandomOrder()->first();
        if (! $get_property) {
            $get_property = Property::factory()->create();
        }

        $property = $this->getContainer()->call(
            [$this->propertyRepository, 'updateById'],
            [
                'id' => $get_property->id,
                'propertyParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getSlug(), $property->slug);
        $this->assertEquals($dummy->getTitle(), $property->title);
        $this->assertEquals($dummy->getDescription(), $property->description);
        $this->assertEquals($dummy->getStatusId(), $property->status_id);
        $this->assertEquals($dummy->getAddress1(), $property->address_1);
        $this->assertEquals($dummy->getAddress2(), $property->address_2);
        $this->assertEquals($dummy->getCity(), $property->city);
        $this->assertEquals($dummy->getState(), $property->state);
        $this->assertEquals($dummy->getZipCode(), $property->zip_code);
        $this->assertEquals($dummy->getLong(), $property->long);
        $this->assertEquals($dummy->getLat(), $property->lat);
        $this->assertEquals($dummy->getUserId(), $property->user_id);
        $this->assertEquals($dummy->getCreatedBy(), $property->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $property->updated_by);
        $this->assertNotEquals(null, $property);
    }

    public function testDeleteById()
    {
        $get_property = Property::factory()->create();

        $property = $this->getContainer()->call(
            [$this->propertyRepository, 'deleteById'],
            [
                'id' => $get_property->id
            ]
        );

        $this->assertTrue($property);
    }

    public function testGetAll()
    {
        $propertys = $this->getContainer()->call(
            [$this->propertyRepository, 'getAll'],
            [

            ]
        );

        $this->assertInstanceOf(Collection::class, $propertys);
    }

    public function testGetById()
    {
        $get_property = Property::inRandomOrder()->first();
        if (! $get_property) {
            $get_property = Property::factory()->create();
        }

        $property = $this->getContainer()->call(
            [$this->propertyRepository, 'getById'],
            [
                'id' => $get_property->id
            ]
        );

        $this->assertNotEquals(null, $property);
        $this->assertEquals($get_property->slug, $property->slug);
        $this->assertEquals($get_property->title, $property->title);
        $this->assertEquals($get_property->description, $property->description);
        $this->assertEquals($get_property->status_id, $property->status_id);
        $this->assertEquals($get_property->address_1, $property->address_1);
        $this->assertEquals($get_property->address_2, $property->address_2);
        $this->assertEquals($get_property->city, $property->city);
        $this->assertEquals($get_property->state, $property->state);
        $this->assertEquals($get_property->zip_code, $property->zip_code);
        $this->assertEquals($get_property->long, $property->long);
        $this->assertEquals($get_property->lat, $property->lat);
        $this->assertEquals($get_property->user_id, $property->user_id);
        $this->assertEquals($get_property->created_by, $property->created_by);
        $this->assertEquals($get_property->updated_by, $property->updated_by);
    }

    public function testDatatable()
    {
        $dummy_datatable = $this->dummyDatatable();

        $records_total = $this->getContainer()->call(
            [$this->propertyRepository, 'getRecordsTotal'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_filtered = $this->getContainer()->call(
            [$this->propertyRepository, 'getRecordsFiltered'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_data = $this->getContainer()->call(
            [$this->propertyRepository, 'getRecordsData'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $this->assertInstanceOf(Collection::class, $records_data);
        $this->assertIsNumeric($records_total);
        $this->assertIsNumeric($records_filtered);
    }
}
