<?php

namespace Tests\Unit\Repositories;

use App\Enums\FileVisibility;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\FileParam;
use App\Http\Repositories\Contract\FileRepositoryContract as FileRepository;
use App\Models\File;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class FileRepositoryTest extends TestCase
{
    private $fileRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->fileRepository = $this->app->make(FileRepository::class);
    }

    public function dummy()
    {
        $fileParam = $this->app->make(FileParam::class);

        $filename = $this->getFaker()->uuid();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $fileParam->setFilename($filename.'.jpg');
        $fileParam->setPath('public/'.$filename.'.jpg');
        $fileParam->setThumbnailPath('public/'.$filename.'.jpg');
        $fileParam->setMimeType('image/jpeg');
        $fileParam->setVisibilityId(FileVisibility::VISIBLE['id']);
        $fileParam->setDescription($this->getFaker()->sentence(10));
        $fileParam->setUserId($user->id);
        $fileParam->setCreatedBy($user->id);
        $fileParam->setUpdatedBy($user->id);

        return $fileParam;
    }

    public function dummyDatatable()
    {
        $generalSearchParam = $this->app->make(GeneralSearchParam::class);

        $generalSearchParam->setQuery('');
        $generalSearchParam->setLength(10);
        $generalSearchParam->setStart(0);
        $generalSearchParam->setOrderColumn('created_at');
        $generalSearchParam->setOrderDirection('desc');

        return $generalSearchParam;
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $file = $this->getContainer()->call(
            [$this->fileRepository, 'create'],
            [
                'fileParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getFilename(), $file->filename);
        $this->assertEquals($dummy->getPath(), $file->path);
        $this->assertEquals($dummy->getThumbnailPath(), $file->thumbnail_path);
        $this->assertEquals($dummy->getMimeType(), $file->mime_type);
        $this->assertEquals($dummy->getVisibilityId(), $file->visibility_id);
        $this->assertEquals($dummy->getDescription(), $file->description);
        $this->assertEquals($dummy->getUserId(), $file->user_id);
        $this->assertEquals($dummy->getCreatedBy(), $file->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $file->updated_by);
        $this->assertNotEquals(null, $file);
    }

    public function testUpdateById()
    {
        $dummy = $this->dummy();

        $get_file = File::inRandomOrder()->first();
        if (! $get_file) {
            $get_file = File::factory()->create();
        }

        $file = $this->getContainer()->call(
            [$this->fileRepository, 'updateById'],
            [
                'id' => $get_file->id,
                'fileParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getFilename(), $file->filename);
        $this->assertEquals($dummy->getPath(), $file->path);
        $this->assertEquals($dummy->getThumbnailPath(), $file->thumbnail_path);
        $this->assertEquals($dummy->getMimeType(), $file->mime_type);
        $this->assertEquals($dummy->getVisibilityId(), $file->visibility_id);
        $this->assertEquals($dummy->getDescription(), $file->description);
        $this->assertEquals($dummy->getUserId(), $file->user_id);
        $this->assertEquals($dummy->getCreatedBy(), $file->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $file->updated_by);
        $this->assertNotEquals(null, $file);
    }

    public function testDeleteById()
    {
        $get_file = File::factory()->create();

        $file = $this->getContainer()->call(
            [$this->fileRepository, 'deleteById'],
            [
                'id' => $get_file->id
            ]
        );

        $this->assertTrue($file);
    }

    public function testGetAll()
    {
        $files = $this->getContainer()->call(
            [$this->fileRepository, 'getAll'],
            [

            ]
        );

        $this->assertInstanceOf(Collection::class, $files);
    }

    public function testGetById()
    {
        $get_file = File::inRandomOrder()->first();
        if (! $get_file) {
            $get_file = File::factory()->create();
        }

        $file = $this->getContainer()->call(
            [$this->fileRepository, 'getById'],
            [
                'id' => $get_file->id
            ]
        );

        $this->assertNotEquals(null, $file);
        $this->assertEquals($get_file->filename, $file->filename);
        $this->assertEquals($get_file->path, $file->path);
        $this->assertEquals($get_file->thumbnail_path, $file->thumbnail_path);
        $this->assertEquals($get_file->mime_type, $file->mime_type);
        $this->assertEquals($get_file->visibility_id, $file->visibility_id);
        $this->assertEquals($get_file->description, $file->description);
        $this->assertEquals($get_file->user_id, $file->user_id);
        $this->assertEquals($get_file->created_by, $file->created_by);
        $this->assertEquals($get_file->updated_by, $file->updated_by);
    }

    public function testDatatable()
    {
        $dummy_datatable = $this->dummyDatatable();

        $records_total = $this->getContainer()->call(
            [$this->fileRepository, 'getRecordsTotal'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_filtered = $this->getContainer()->call(
            [$this->fileRepository, 'getRecordsFiltered'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_data = $this->getContainer()->call(
            [$this->fileRepository, 'getRecordsData'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $this->assertInstanceOf(Collection::class, $records_data);
        $this->assertIsNumeric($records_total);
        $this->assertIsNumeric($records_filtered);
    }
}
