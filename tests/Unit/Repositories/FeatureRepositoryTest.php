<?php

namespace Tests\Unit\Repositories;

use App\Http\Params\GeneralSearchParam;
use App\Http\Params\FeatureParam;
use App\Http\Repositories\Contract\FeatureRepositoryContract as FeatureRepository;
use App\Models\Feature;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class FeatureRepositoryTest extends TestCase
{
    private $featureRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->featureRepository = $this->app->make(FeatureRepository::class);
    }

    public function dummy()
    {
        $featureParam = $this->app->make(FeatureParam::class);

        $name = $this->getFaker()->name();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $featureParam->setSlug(Str::slug($name));
        $featureParam->setName($name);
        $featureParam->setCreatedBy($user->id);
        $featureParam->setUpdatedBy($user->id);

        return $featureParam;
    }

    public function dummyDatatable()
    {
        $generalSearchParam = $this->app->make(GeneralSearchParam::class);

        $generalSearchParam->setQuery('');
        $generalSearchParam->setLength(10);
        $generalSearchParam->setStart(0);
        $generalSearchParam->setOrderColumn('created_at');
        $generalSearchParam->setOrderDirection('desc');

        return $generalSearchParam;
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $feature = $this->getContainer()->call(
            [$this->featureRepository, 'create'],
            [
                'featureParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getSlug(), $feature->slug);
        $this->assertEquals($dummy->getName(), $feature->name);
        $this->assertEquals($dummy->getCreatedBy(), $feature->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $feature->updated_by);
        $this->assertNotEquals(null, $feature);
    }

    public function testUpdateById()
    {
        $dummy = $this->dummy();

        $get_feature = Feature::inRandomOrder()->first();
        if (! $get_feature) {
            $get_feature = Feature::factory()->create();
        }

        $feature = $this->getContainer()->call(
            [$this->featureRepository, 'updateById'],
            [
                'id' => $get_feature->id,
                'featureParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getSlug(), $feature->slug);
        $this->assertEquals($dummy->getName(), $feature->name);
        $this->assertEquals($dummy->getCreatedBy(), $feature->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $feature->updated_by);
        $this->assertNotEquals(null, $feature);
    }

    public function testDeleteById()
    {
        $get_feature = Feature::factory()->create();

        $feature = $this->getContainer()->call(
            [$this->featureRepository, 'deleteById'],
            [
                'id' => $get_feature->id
            ]
        );

        $this->assertTrue($feature);
    }

    public function testGetAll()
    {
        $features = $this->getContainer()->call(
            [$this->featureRepository, 'getAll'],
            [

            ]
        );

        $this->assertInstanceOf(Collection::class, $features);
    }

    public function testGetById()
    {
        $get_feature = Feature::inRandomOrder()->first();
        if (! $get_feature) {
            $get_feature = Feature::factory()->create();
        }

        $feature = $this->getContainer()->call(
            [$this->featureRepository, 'getById'],
            [
                'id' => $get_feature->id
            ]
        );

        $this->assertNotEquals(null, $feature);
        $this->assertEquals($get_feature->slug, $feature->slug);
        $this->assertEquals($get_feature->name, $feature->name);
        $this->assertEquals($get_feature->created_by, $feature->created_by);
        $this->assertEquals($get_feature->updated_by, $feature->updated_by);
    }

    public function testDatatable()
    {
        $dummy_datatable = $this->dummyDatatable();

        $records_total = $this->getContainer()->call(
            [$this->featureRepository, 'getRecordsTotal'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_filtered = $this->getContainer()->call(
            [$this->featureRepository, 'getRecordsFiltered'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_data = $this->getContainer()->call(
            [$this->featureRepository, 'getRecordsData'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $this->assertInstanceOf(Collection::class, $records_data);
        $this->assertIsNumeric($records_total);
        $this->assertIsNumeric($records_filtered);
    }
}
