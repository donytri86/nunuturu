<?php

namespace Tests\Unit\Repositories;

use App\Enums\UserStatus;
use App\Http\Params\GeneralSearchParam;
use App\Http\Params\UserParam;
use App\Http\Repositories\Contract\UserRepositoryContract as UserRepository;
use App\Models\User;
use Illuminate\Support\Collection;
use Tests\TestCase;

class UserRepositoryTest extends TestCase
{
    private $userRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->userRepository = $this->app->make(UserRepository::class);
    }

    public function dummy()
    {
        $userParam = $this->app->make(UserParam::class);

        $firstname = $this->getFaker()->firstName();
        $lastname = $this->getFaker()->lastName();
        $username = $this->getFaker()->userName();
        $email = $this->getFaker()->safeEmail();
        $status_id = UserStatus::ACTIVE['id'];
        $password = bcrypt('password');

        $userParam->setFirstName($firstname);
        $userParam->setLastName($lastname);
        $userParam->setEmail($email);
        $userParam->setStatusId($status_id);
        $userParam->setEmailVerifiedAt(now());
        $userParam->setPassword($password);

        return $userParam;
    }

    public function dummyDatatable()
    {
        $generalSearchParam = $this->app->make(GeneralSearchParam::class);

        $generalSearchParam->setQuery('');
        $generalSearchParam->setLength(10);
        $generalSearchParam->setStart(0);
        $generalSearchParam->setOrderColumn('created_at');
        $generalSearchParam->setOrderDirection('desc');

        return $generalSearchParam;
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $user = $this->getContainer()->call(
            [$this->userRepository, 'create'],
            [
                'userParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getFirstName(), $user->first_name);
        $this->assertEquals($dummy->getLastName(), $user->last_name);
        $this->assertEquals($dummy->getEmail(), $user->email);
        $this->assertEquals($dummy->getStatusId(), $user->status_id);
        $this->assertNotEquals(null, $user);
    }

    public function testUpdateById()
    {
        $dummy = $this->dummy();

        $get_user = User::inRandomOrder()->first();
        if (! $get_user) {
            $get_user = User::factory()->create();
        }

        $user = $this->getContainer()->call(
            [$this->userRepository, 'updateById'],
            [
                'id' => $get_user->id,
                'userParam' => $dummy
            ]
        );

        $this->assertEquals($dummy->getFirstName(), $user->first_name);
        $this->assertEquals($dummy->getLastName(), $user->last_name);
        $this->assertEquals($dummy->getEmail(), $user->email);
        $this->assertEquals($dummy->getStatusId(), $user->status_id);
        $this->assertNotEquals(null, $user);
    }

    public function testDeleteById()
    {
        $get_user = User::factory()->create();

        $user = $this->getContainer()->call(
            [$this->userRepository, 'deleteById'],
            [
                'id' => $get_user->id
            ]
        );

        $this->assertTrue($user);
    }

    public function testGetAll()
    {
        $users = $this->getContainer()->call(
            [$this->userRepository, 'getAll'],
            [

            ]
        );

        $this->assertInstanceOf(Collection::class, $users);
    }

    public function testGetById()
    {
        $get_user = User::inRandomOrder()->first();
        if (! $get_user) {
            $get_user = User::factory()->create();
        }

        $user = $this->getContainer()->call(
            [$this->userRepository, 'getById'],
            [
                'id' => $get_user->id
            ]
        );

        $this->assertNotEquals(null, $user);
        $this->assertEquals($get_user->first_name, $user->first_name);
        $this->assertEquals($get_user->last_name, $user->last_name);
        $this->assertEquals($get_user->username, $user->username);
        $this->assertEquals($get_user->email, $user->email);
        $this->assertEquals($get_user->status_id, $user->status_id);
    }

    public function testDatatable()
    {
        $dummy_datatable = $this->dummyDatatable();

        $records_total = $this->getContainer()->call(
            [$this->userRepository, 'getRecordsTotal'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_filtered = $this->getContainer()->call(
            [$this->userRepository, 'getRecordsFiltered'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $records_data = $this->getContainer()->call(
            [$this->userRepository, 'getRecordsData'],
            [
                'generalSearchParam' => $dummy_datatable
            ]
        );

        $this->assertInstanceOf(Collection::class, $records_data);
        $this->assertIsNumeric($records_total);
        $this->assertIsNumeric($records_filtered);
    }
}
