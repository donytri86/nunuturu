<?php

namespace Tests\Unit\Services;

use App\Enums\Contact;
use App\Enums\UserStatus;
use App\Http\Params\ArrayParam;
use App\Http\Params\UserContactParam;
use App\Http\Params\UserParam;
use App\Http\Services\Contract\UserServiceContract as UserService;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    private $userService;

    public function setUp(): void
    {
        parent::setUp();

        $this->userService = $this->app->make(UserService::class);
    }

    public function dummy()
    {
        $userParam = $this->app->make(UserParam::class);

        $firstname = $this->getFaker()->firstName();
        $lastname = $this->getFaker()->lastName();
        $username = $this->getFaker()->userName();
        $email = $this->getFaker()->safeEmail();
        $status_id = UserStatus::ACTIVE['id'];
        $password = bcrypt('password');

        $userParam->setFirstName($firstname);
        $userParam->setLastName($lastname);
        $userParam->setEmail($email);
        $userParam->setStatusId($status_id);
        $userParam->setEmailVerifiedAt(now());
        $userParam->setPassword($password);

        return $userParam;
    }

    public function dummyContact()
    {
        $userContactParam = $this->app->make(UserContactParam::class);

        $name = $this->getFaker()->name();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $userContactParam->setUserId($user->id);
        $userContactParam->setContactId(Contact::PHONE['id']);
        $userContactParam->setContact($this->getFaker()->phoneNumber());
        $userContactParam->setCreatedBy($user->id);
        $userContactParam->setUpdatedBy($user->id);

        return $userContactParam;
    }

    public function dummyContacts()
    {
        $arrayParam = $this->app->make(ArrayParam::class);

        $arrayParam->add($this->dummyContact());
        $arrayParam->add($this->dummyContact());

        return $arrayParam;
    }

    public function testList()
    {
        $userService = $this->getContainer()->call(
            [$this->userService, 'list'],
            []
        );

        $this->assertNotInstanceOf('Exception', $userService);

        $this->assertInstanceOf(Collection::class, $userService->data);
        $this->assertIsNumeric($userService->total);
        $this->assertIsNumeric($userService->filtered);
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $userService = $this->getContainer()->call(
            [$this->userService, 'create'],
            [
                'userParam' => $dummy
            ]
        );

        $this->assertNotInstanceOf('Exception', $userService);

        $this->assertEquals($dummy->getFirstName(), $userService->data->first_name);
        $this->assertEquals($dummy->getLastName(), $userService->data->last_name);
        $this->assertEquals($dummy->getEmail(), $userService->data->email);
        $this->assertEquals($dummy->getStatusId(), $userService->data->status_id);
        $this->assertNotEquals(null, $userService);
    }

    public function testUpdate()
    {
        $dummy = $this->dummy();
        $dummyContacts = $this->dummyContacts();
        $dummyContactsCount = $dummyContacts->count();

        $get_user = User::inRandomOrder()->first();
        if (! $get_user) {
            $get_user = User::factory()->create();
        }

        $userService = $this->getContainer()->call(
            [$this->userService, 'update'],
            [
                'id' => $get_user->id,
                'userParam' => $dummy,
                'userContactParams' => $dummyContacts,
            ]
        );

        $this->assertNotInstanceOf('Exception', $userService);

        $this->assertEquals($dummy->getFirstName(), $userService->data->first_name);
        $this->assertEquals($dummy->getLastName(), $userService->data->last_name);
        $this->assertEquals($dummy->getEmail(), $userService->data->email);
        $this->assertEquals($dummy->getStatusId(), $userService->data->status_id);
        $this->assertEquals($dummyContactsCount, $userService->data->contacts->count());
        $this->assertNotEquals(null, $userService);
    }

    public function testUpdatePassword()
    {
        $dummy = $this->dummy();
        $dummy->setCurrentPassword('password');
        $dummy->setPassword('newpassword');

        $get_user = User::factory()->create();

        $userService = $this->getContainer()->call(
            [$this->userService, 'updatePassword'],
            [
                'id' => $get_user->id,
                'userParam' => $dummy,
            ]
        );

        $this->assertNotInstanceOf('Exception', $userService);

        $check_old_hash = Hash::check($dummy->getCurrentPassword(), $userService->data->password);
        $check_new_hash = Hash::check($dummy->getPassword(), $userService->data->password);

        $this->assertFalse($check_old_hash);
        $this->assertTrue($check_new_hash);

        $this->assertEquals($get_user->first_name, $userService->data->first_name);
        $this->assertEquals($get_user->last_name, $userService->data->last_name);
        $this->assertEquals($get_user->email, $userService->data->email);
        $this->assertEquals($get_user->status_id, $userService->data->status_id);
        $this->assertNotEquals(null, $userService);
    }

    public function testShow()
    {
        $get_user = User::inRandomOrder()->first();
        if (! $get_user) {
            $get_user = User::factory()->create();
        }

        $userService = $this->getContainer()->call(
            [$this->userService, 'show'],
            [
                'id' => $get_user->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $userService);

        $this->assertEquals($get_user->first_name, $userService->data->first_name);
        $this->assertEquals($get_user->last_name, $userService->data->last_name);
        $this->assertEquals($get_user->username, $userService->data->username);
        $this->assertEquals($get_user->email, $userService->data->email);
        $this->assertEquals($get_user->status_id, $userService->data->status_id);
    }

    public function testDelete()
    {
        $get_user = User::factory()->create();

        $userService = $this->getContainer()->call(
            [$this->userService, 'delete'],
            [
                'id' => $get_user->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $userService);
        $this->assertTrue($userService->data);
    }
}
