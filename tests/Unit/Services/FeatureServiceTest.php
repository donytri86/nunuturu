<?php

namespace Tests\Unit\Services;

use App\Http\Params\FeatureParam;
use App\Http\Services\Contract\FeatureServiceContract as FeatureService;
use App\Models\Feature;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class FeatureServiceTest extends TestCase
{
    private $featureService;

    public function setUp(): void
    {
        parent::setUp();

        $this->featureService = $this->app->make(FeatureService::class);
    }

    public function dummy()
    {
        $featureParam = $this->app->make(FeatureParam::class);

        $name = $this->getFaker()->name();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $featureParam->setSlug(Str::slug($name));
        $featureParam->setName($name);
        $featureParam->setCreatedBy($user->id);
        $featureParam->setUpdatedBy($user->id);

        return $featureParam;
    }

    public function testList()
    {
        $featureService = $this->getContainer()->call(
            [$this->featureService, 'list'],
            []
        );

        $this->assertNotInstanceOf('Exception', $featureService);

        $this->assertInstanceOf(Collection::class, $featureService->data);
        $this->assertIsNumeric($featureService->total);
        $this->assertIsNumeric($featureService->filtered);
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $featureService = $this->getContainer()->call(
            [$this->featureService, 'create'],
            [
                'featureParam' => $dummy
            ]
        );

        $this->assertNotInstanceOf('Exception', $featureService);

        $this->assertEquals($dummy->getSlug(), $featureService->data->slug);
        $this->assertEquals($dummy->getName(), $featureService->data->name);
        $this->assertEquals($dummy->getCreatedBy(), $featureService->data->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $featureService->data->updated_by);
        $this->assertNotEquals(null, $featureService);
    }

    public function testUpdate()
    {
        $dummy = $this->dummy();

        $get_feature = Feature::inRandomOrder()->first();
        if (! $get_feature) {
            $get_feature = Feature::factory()->create();
        }

        $featureService = $this->getContainer()->call(
            [$this->featureService, 'update'],
            [
                'id' => $get_feature->id,
                'featureParam' => $dummy,
            ]
        );

        $this->assertNotInstanceOf('Exception', $featureService);

        $this->assertEquals($dummy->getSlug(), $featureService->data->slug);
        $this->assertEquals($dummy->getName(), $featureService->data->name);
        $this->assertEquals($dummy->getCreatedBy(), $featureService->data->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $featureService->data->updated_by);
        $this->assertNotEquals(null, $featureService);
    }

    public function testShow()
    {
        $get_feature = Feature::inRandomOrder()->first();
        if (! $get_feature) {
            $get_feature = Feature::factory()->create();
        }

        $featureService = $this->getContainer()->call(
            [$this->featureService, 'show'],
            [
                'id' => $get_feature->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $featureService);

        $this->assertEquals($get_feature->slug, $featureService->data->slug);
        $this->assertEquals($get_feature->name, $featureService->data->name);
        $this->assertEquals($get_feature->created_by, $featureService->data->created_by);
        $this->assertEquals($get_feature->updated_by, $featureService->data->updated_by);
    }

    public function testDelete()
    {
        $get_feature = Feature::factory()->create();

        $featureService = $this->getContainer()->call(
            [$this->featureService, 'delete'],
            [
                'id' => $get_feature->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $featureService);
        $this->assertTrue($featureService->data);
    }
}
