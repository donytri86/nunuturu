<?php

namespace Tests\Unit\Services;

use App\Enums\Period;
use App\Enums\PropertyStatus;
use App\Http\Params\ArrayParam;
use App\Http\Params\PropertyParam;
use App\Http\Params\PropertyPriceParam;
use App\Http\Services\Contract\PropertyServiceContract as PropertyService;
use App\Models\File;
use App\Models\Property;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class PropertyServiceTest extends TestCase
{
    private $propertyService;

    public function setUp(): void
    {
        parent::setUp();

        $this->propertyService = $this->app->make(PropertyService::class);
    }

    public function dummy()
    {
        $propertyParam = $this->app->make(PropertyParam::class);

        $title = $this->getFaker()->company();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $files = File::factory()->count(3)->create();

        $propertyParam->setSlug(Str::slug($title));
        $propertyParam->setTitle($title);
        $propertyParam->setDescription($this->getFaker()->sentence(10));
        $propertyParam->setStatusId(PropertyStatus::AVAILABLE['id']);
        $propertyParam->setAddress1($this->getFaker()->streetAddress());
        $propertyParam->setAddress2($this->getFaker()->address());
        $propertyParam->setCity($this->getFaker()->city());
        $propertyParam->setState($this->getFaker()->state());
        $propertyParam->setZipCode($this->getFaker()->postcode());
        $propertyParam->setLong($this->getFaker()->longitude());
        $propertyParam->setLat($this->getFaker()->latitude());
        $propertyParam->setUserId($user->id);
        $propertyParam->setCreatedBy($user->id);
        $propertyParam->setUpdatedBy($user->id);
        $propertyParam->setGalleryIds($files->pluck('id')->all());

        return $propertyParam;
    }

    public function dummyPrice()
    {
        $propertyPriceParam = $this->app->make(PropertyPriceParam::class);

        $name = $this->getFaker()->name();

        $property = Property::inRandomOrder()->first();
        if (! $property) {
            $property = Property::factory()->create();
        }

        $propertyPriceParam->setPropertyId($property->id);
        $propertyPriceParam->setPeriodId(Period::MONTHLY['id']);
        $propertyPriceParam->setPrice($this->getFaker()->numberBetween(500, 3000));

        return $propertyPriceParam;
    }

    public function dummyPrices()
    {
        $arrayParam = $this->app->make(ArrayParam::class);

        return $arrayParam;
    }

    public function testList()
    {
        $propertyService = $this->getContainer()->call(
            [$this->propertyService, 'list'],
            []
        );

        $this->assertNotInstanceOf('Exception', $propertyService);

        $this->assertInstanceOf(Collection::class, $propertyService->data);
        $this->assertIsNumeric($propertyService->total);
        $this->assertIsNumeric($propertyService->filtered);
    }

    public function testCreate()
    {
        $dummy = $this->dummy();
        $dummyPrices = $this->dummyPrices();

        $dummyPrices->add($this->dummyPrice());
        $dummyPrices->add($this->dummyPrice());

        $dummyPricesCount = $dummyPrices->count();

        $propertyService = $this->getContainer()->call(
            [$this->propertyService, 'create'],
            [
                'propertyParam' => $dummy,
                'propertyPriceParams' => $dummyPrices
            ]
        );

        $this->assertNotInstanceOf('Exception', $propertyService);

        $this->assertEquals($dummy->getSlug(), $propertyService->data->slug);
        $this->assertEquals($dummy->getTitle(), $propertyService->data->title);
        $this->assertEquals($dummy->getDescription(), $propertyService->data->description);
        $this->assertEquals($dummy->getStatusId(), $propertyService->data->status_id);
        $this->assertEquals($dummy->getAddress1(), $propertyService->data->address_1);
        $this->assertEquals($dummy->getAddress2(), $propertyService->data->address_2);
        $this->assertEquals($dummy->getCity(), $propertyService->data->city);
        $this->assertEquals($dummy->getState(), $propertyService->data->state);
        $this->assertEquals($dummy->getZipCode(), $propertyService->data->zip_code);
        $this->assertEquals($dummy->getLong(), $propertyService->data->long);
        $this->assertEquals($dummy->getLat(), $propertyService->data->lat);
        $this->assertEquals($dummy->getUserId(), $propertyService->data->user_id);
        $this->assertEquals($dummy->getCreatedBy(), $propertyService->data->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $propertyService->data->updated_by);
        $this->assertEquals($dummy->getGalleryIds(), $propertyService->data->galleries->pluck('id')->all());
        $this->assertEquals($dummyPricesCount, $propertyService->data->prices->count());
        $this->assertNotEquals(null, $propertyService);
    }

    public function testUpdate()
    {
        $dummy = $this->dummy();
        $dummyPrices = $this->dummyPrices();

        $dummyPrices->add($this->dummyPrice());
        $dummyPrices->add($this->dummyPrice());

        $dummyPricesCount = $dummyPrices->count();

        $get_property = Property::inRandomOrder()->first();
        if (! $get_property) {
            $get_property = Property::factory()->create();
        }

        $propertyService = $this->getContainer()->call(
            [$this->propertyService, 'update'],
            [
                'id' => $get_property->id,
                'propertyParam' => $dummy,
                'propertyPriceParams' => $dummyPrices,
            ]
        );

        $this->assertNotInstanceOf('Exception', $propertyService);

        $this->assertEquals($dummy->getSlug(), $propertyService->data->slug);
        $this->assertEquals($dummy->getTitle(), $propertyService->data->title);
        $this->assertEquals($dummy->getDescription(), $propertyService->data->description);
        $this->assertEquals($dummy->getStatusId(), $propertyService->data->status_id);
        $this->assertEquals($dummy->getAddress1(), $propertyService->data->address_1);
        $this->assertEquals($dummy->getAddress2(), $propertyService->data->address_2);
        $this->assertEquals($dummy->getCity(), $propertyService->data->city);
        $this->assertEquals($dummy->getState(), $propertyService->data->state);
        $this->assertEquals($dummy->getZipCode(), $propertyService->data->zip_code);
        $this->assertEquals($dummy->getLong(), $propertyService->data->long);
        $this->assertEquals($dummy->getLat(), $propertyService->data->lat);
        $this->assertEquals($dummy->getUserId(), $propertyService->data->user_id);
        $this->assertEquals($dummy->getCreatedBy(), $propertyService->data->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $propertyService->data->updated_by);
        $this->assertEquals($dummy->getGalleryIds(), $propertyService->data->galleries->pluck('id')->all());
        $this->assertEquals($dummyPricesCount, $propertyService->data->prices->count());
        $this->assertNotEquals(null, $propertyService);
    }

    public function testShow()
    {
        $get_property = Property::inRandomOrder()->first();
        if (! $get_property) {
            $get_property = Property::factory()->create();
        }

        $propertyService = $this->getContainer()->call(
            [$this->propertyService, 'show'],
            [
                'id' => $get_property->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $propertyService);

        $this->assertEquals($get_property->first_name, $propertyService->data->first_name);
        $this->assertEquals($get_property->last_name, $propertyService->data->last_name);
        $this->assertEquals($get_property->propertyname, $propertyService->data->propertyname);
        $this->assertEquals($get_property->email, $propertyService->data->email);
        $this->assertEquals($get_property->status_id, $propertyService->data->status_id);
    }

    public function testDelete()
    {
        $get_property = Property::factory()->create();

        $propertyService = $this->getContainer()->call(
            [$this->propertyService, 'delete'],
            [
                'id' => $get_property->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $propertyService);
        $this->assertTrue($propertyService->data);
    }
}
