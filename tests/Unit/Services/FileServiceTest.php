<?php

namespace Tests\Unit\Services;

use App\Enums\FileVisibility;
use App\Http\Params\FileParam;
use App\Http\Services\Contract\FileServiceContract as FileService;
use App\Models\File;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class FileServiceTest extends TestCase
{
    private $fileService;

    public function setUp(): void
    {
        parent::setUp();

        $this->fileService = $this->app->make(FileService::class);
    }

    public function dummy()
    {
        $fileParam = $this->app->make(FileParam::class);

        $filename = $this->getFaker()->uuid();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $fileParam->setFilename($filename.'.jpg');
        $fileParam->setPath('public/'.$filename.'.jpg');
        $fileParam->setThumbnailPath('public/'.$filename.'.jpg');
        $fileParam->setMimeType('image/jpeg');
        $fileParam->setVisibilityId(FileVisibility::VISIBLE['id']);
        $fileParam->setDescription($this->getFaker()->sentence(10));
        $fileParam->setUserId($user->id);
        $fileParam->setCreatedBy($user->id);
        $fileParam->setUpdatedBy($user->id);

        return $fileParam;
    }

    public function testList()
    {
        $fileService = $this->getContainer()->call(
            [$this->fileService, 'list'],
            []
        );

        $this->assertNotInstanceOf('Exception', $fileService);

        $this->assertInstanceOf(Collection::class, $fileService->data);
        $this->assertIsNumeric($fileService->total);
        $this->assertIsNumeric($fileService->filtered);
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $fileService = $this->getContainer()->call(
            [$this->fileService, 'create'],
            [
                'fileParam' => $dummy
            ]
        );

        $this->assertNotInstanceOf('Exception', $fileService);

        $this->assertEquals($dummy->getFilename(), $fileService->data->filename);
        $this->assertEquals($dummy->getPath(), $fileService->data->path);
        $this->assertEquals($dummy->getThumbnailPath(), $fileService->data->thumbnail_path);
        $this->assertEquals($dummy->getMimeType(), $fileService->data->mime_type);
        $this->assertEquals($dummy->getVisibilityId(), $fileService->data->visibility_id);
        $this->assertEquals($dummy->getDescription(), $fileService->data->description);
        $this->assertEquals($dummy->getUserId(), $fileService->data->user_id);
        $this->assertEquals($dummy->getCreatedBy(), $fileService->data->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $fileService->data->updated_by);
        $this->assertNotEquals(null, $fileService);
    }

    public function testUpdate()
    {
        $dummy = $this->dummy();

        $get_file = File::inRandomOrder()->first();
        if (! $get_file) {
            $get_file = File::factory()->create();
        }

        $fileService = $this->getContainer()->call(
            [$this->fileService, 'update'],
            [
                'id' => $get_file->id,
                'fileParam' => $dummy,
            ]
        );

        $this->assertNotInstanceOf('Exception', $fileService);

        $this->assertEquals($dummy->getFilename(), $fileService->data->filename);
        $this->assertEquals($dummy->getPath(), $fileService->data->path);
        $this->assertEquals($dummy->getThumbnailPath(), $fileService->data->thumbnail_path);
        $this->assertEquals($dummy->getMimeType(), $fileService->data->mime_type);
        $this->assertEquals($dummy->getVisibilityId(), $fileService->data->visibility_id);
        $this->assertEquals($dummy->getDescription(), $fileService->data->description);
        $this->assertEquals($dummy->getUserId(), $fileService->data->user_id);
        $this->assertEquals($dummy->getCreatedBy(), $fileService->data->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $fileService->data->updated_by);
        $this->assertNotEquals(null, $fileService);
    }

    public function testShow()
    {
        $get_file = File::inRandomOrder()->first();
        if (! $get_file) {
            $get_file = File::factory()->create();
        }

        $fileService = $this->getContainer()->call(
            [$this->fileService, 'show'],
            [
                'id' => $get_file->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $fileService);

        $this->assertEquals($get_file->filename, $fileService->data->filename);
        $this->assertEquals($get_file->path, $fileService->data->path);
        $this->assertEquals($get_file->thumbnail_path, $fileService->data->thumbnail_path);
        $this->assertEquals($get_file->mime_type, $fileService->data->mime_type);
        $this->assertEquals($get_file->visibility_id, $fileService->data->visibility_id);
        $this->assertEquals($get_file->description, $fileService->data->description);
        $this->assertEquals($get_file->user_id, $fileService->data->user_id);
        $this->assertEquals($get_file->created_by, $fileService->data->created_by);
        $this->assertEquals($get_file->updated_by, $fileService->data->updated_by);
    }

    public function testDelete()
    {
        $get_file = File::factory()->create();

        $fileService = $this->getContainer()->call(
            [$this->fileService, 'delete'],
            [
                'id' => $get_file->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $fileService);
        $this->assertTrue($fileService->data);
    }
}
