<?php

namespace Tests\Unit\Services;

use App\Enums\Period;
use App\Http\Params\RentParam;
use App\Http\Services\Contract\RentServiceContract as RentService;
use App\Models\Property;
use App\Models\Rent;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class RentServiceTest extends TestCase
{
    private $rentService;

    public function setUp(): void
    {
        parent::setUp();

        $this->rentService = $this->app->make(RentService::class);
    }

    public function dummy()
    {
        $rentParam = $this->app->make(RentParam::class);

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $property = Property::inRandomOrder()->first();
        if (! $property) {
            $property = User::factory()->create();
        }

        $rentParam->setPropertyId($property->id);
        $rentParam->setUserId($user->id);
        $rentParam->setRentDate(now());
        $rentParam->setRentPeriod(Period::MONTHLY['id']);
        $rentParam->setPeriodId($this->getFaker()->numberBetween(1, 6));
        $rentParam->setPrice($this->getFaker()->numberBetween(500, 3000));

        return $rentParam;
    }

    public function testList()
    {
        $rentService = $this->getContainer()->call(
            [$this->rentService, 'list'],
            []
        );

        $this->assertNotInstanceOf('Exception', $rentService);

        $this->assertInstanceOf(Collection::class, $rentService->data);
        $this->assertIsNumeric($rentService->total);
        $this->assertIsNumeric($rentService->filtered);
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $rentService = $this->getContainer()->call(
            [$this->rentService, 'create'],
            [
                'rentParam' => $dummy
            ]
        );

        $this->assertNotInstanceOf('Exception', $rentService);

        $this->assertEquals($dummy->getPropertyId(), $rentService->data->property_id);
        $this->assertEquals($dummy->getUserId(), $rentService->data->user_id);
        $this->assertEquals($dummy->getRentDate(), $rentService->data->rent_date);
        $this->assertEquals($dummy->getRentPeriod(), $rentService->data->rent_period);
        $this->assertEquals($dummy->getPeriodId(), $rentService->data->period_id);
        $this->assertEquals($dummy->getPrice(), $rentService->data->price);
        $this->assertNotEquals(null, $rentService);
    }

    public function testUpdate()
    {
        $dummy = $this->dummy();

        $get_rent = Rent::inRandomOrder()->first();
        if (! $get_rent) {
            $get_rent = Rent::factory()->create();
        }

        $rentService = $this->getContainer()->call(
            [$this->rentService, 'update'],
            [
                'id' => $get_rent->id,
                'rentParam' => $dummy,
            ]
        );

        $this->assertNotInstanceOf('Exception', $rentService);

        $this->assertEquals($dummy->getPropertyId(), $rentService->data->property_id);
        $this->assertEquals($dummy->getUserId(), $rentService->data->user_id);
        $this->assertEquals($dummy->getRentDate(), $rentService->data->rent_date);
        $this->assertEquals($dummy->getRentPeriod(), $rentService->data->rent_period);
        $this->assertEquals($dummy->getPeriodId(), $rentService->data->period_id);
        $this->assertEquals($dummy->getPrice(), $rentService->data->price);
        $this->assertNotEquals(null, $rentService);
    }

    public function testShow()
    {
        $get_rent = Rent::inRandomOrder()->first();
        if (! $get_rent) {
            $get_rent = Rent::factory()->create();
        }

        $rentService = $this->getContainer()->call(
            [$this->rentService, 'show'],
            [
                'id' => $get_rent->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $rentService);

        $this->assertEquals($get_rent->property_id, $rentService->data->property_id);
        $this->assertEquals($get_rent->user_id, $rentService->data->user_id);
        $this->assertEquals($get_rent->rent_date, $rentService->data->rent_date);
        $this->assertEquals($get_rent->rent_period, $rentService->data->rent_period);
        $this->assertEquals($get_rent->period_id, $rentService->data->period_id);
        $this->assertEquals($get_rent->price, $rentService->data->price);
    }

    public function testDelete()
    {
        $get_rent = Rent::factory()->create();

        $rentService = $this->getContainer()->call(
            [$this->rentService, 'delete'],
            [
                'id' => $get_rent->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $rentService);
        $this->assertTrue($rentService->data);
    }
}
