<?php

namespace Tests\Unit\Services;

use App\Http\Params\RoleParam;
use App\Http\Services\Contract\RoleServiceContract as RoleService;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tests\TestCase;

class RoleServiceTest extends TestCase
{
    private $roleService;

    public function setUp(): void
    {
        parent::setUp();

        $this->roleService = $this->app->make(RoleService::class);
    }

    public function dummy()
    {
        $roleParam = $this->app->make(RoleParam::class);

        $name = $this->getFaker()->name();

        $user = User::inRandomOrder()->first();
        if (! $user) {
            $user = User::factory()->create();
        }

        $roleParam->setSlug(Str::slug($name));
        $roleParam->setName($name);
        $roleParam->setCreatedBy($user->id);
        $roleParam->setUpdatedBy($user->id);

        return $roleParam;
    }

    public function testList()
    {
        $roleService = $this->getContainer()->call(
            [$this->roleService, 'list'],
            []
        );

        $this->assertNotInstanceOf('Exception', $roleService);

        $this->assertInstanceOf(Collection::class, $roleService->data);
        $this->assertIsNumeric($roleService->total);
        $this->assertIsNumeric($roleService->filtered);
    }

    public function testCreate()
    {
        $dummy = $this->dummy();

        $roleService = $this->getContainer()->call(
            [$this->roleService, 'create'],
            [
                'roleParam' => $dummy
            ]
        );

        $this->assertNotInstanceOf('Exception', $roleService);

        $this->assertEquals($dummy->getSlug(), $roleService->data->slug);
        $this->assertEquals($dummy->getName(), $roleService->data->name);
        $this->assertEquals($dummy->getCreatedBy(), $roleService->data->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $roleService->data->updated_by);
        $this->assertNotEquals(null, $roleService);
    }

    public function testUpdate()
    {
        $dummy = $this->dummy();

        $get_role = Role::inRandomOrder()->first();
        if (! $get_role) {
            $get_role = Role::factory()->create();
        }

        $roleService = $this->getContainer()->call(
            [$this->roleService, 'update'],
            [
                'id' => $get_role->id,
                'roleParam' => $dummy,
            ]
        );

        $this->assertNotInstanceOf('Exception', $roleService);

        $this->assertEquals($dummy->getSlug(), $roleService->data->slug);
        $this->assertEquals($dummy->getName(), $roleService->data->name);
        $this->assertEquals($dummy->getCreatedBy(), $roleService->data->created_by);
        $this->assertEquals($dummy->getUpdatedBy(), $roleService->data->updated_by);
        $this->assertNotEquals(null, $roleService);
    }

    public function testShow()
    {
        $get_role = Role::inRandomOrder()->first();
        if (! $get_role) {
            $get_role = Role::factory()->create();
        }

        $roleService = $this->getContainer()->call(
            [$this->roleService, 'show'],
            [
                'id' => $get_role->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $roleService);

        $this->assertEquals($get_role->slug, $roleService->data->slug);
        $this->assertEquals($get_role->name, $roleService->data->name);
        $this->assertEquals($get_role->created_by, $roleService->data->created_by);
        $this->assertEquals($get_role->updated_by, $roleService->data->updated_by);
    }

    public function testDelete()
    {
        $get_role = Role::factory()->create();

        $roleService = $this->getContainer()->call(
            [$this->roleService, 'delete'],
            [
                'id' => $get_role->id,
            ]
        );

        $this->assertNotInstanceOf('Exception', $roleService);
        $this->assertTrue($roleService->data);
    }
}
